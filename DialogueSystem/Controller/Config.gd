extends Node

## Сохраненные массивы и классы для дефолтных значений
## нужны чтобы не выделять память на них при загрузке пулов объектов
var DEFAULT_NEEDS_ARRAY:PoolIntArray = [0,0,0,0,0,
										0,0,0,0,0];
var DEFAULT_QUALITY:Qualities = Qualities.new(DEFAULT_NEEDS_ARRAY);
var FUNCTION_REGEX:RegEx = RegEx.new();
var DEFAULT_DIRECTORY:Directory = Directory.new();

func _ready()->void:
	_knowledge.create(Vector2(1024,64));
	if FUNCTION_REGEX.compile('{[@#$%A-Za-z0-9_.()]+}') != OK:
		print('message regex not load');

## константы конфигов
const PLAYER_ANCHOR:String = '@p';
const PLAYER_POSITION:String = '#p';
const INTERLOCUTOR_ANCHOR:String = '@i'
const MONEY_ANCHOR:String = '&m';

## константы путей к файлам
const CHARACTER_IMAGE_PATH:String = 'res://Res/Character/';
const STUFF_IMAGE_PATH:String = 'res://Res/Stuff/';
const LOCATION_IMAGE_PATH:String = 'res://Res/Location/';

## ## эти действия вызовутся сразу после старта игры
var _START_ACTIONS:Array = [];
func START_ACTIONS_SET(value:Array)->void:
	_START_ACTIONS = value;
func START_ACTION_GET()->Array:
	return _START_ACTIONS;


enum CONTROLLERS {
	NONE,
	SOUL,
	GOAP,
}

## Тип контроллера нужд
var _NEED_CONTROLLER:int=CONTROLLERS.SOUL setget _NEED_CONTROLLER_SET, NEED_CONTROLLER_GET;
func _NEED_CONTROLLER_SET(value:int)->void:
	_NEED_CONTROLLER = value;
func NEED_CONTROLLER_GET()->int:
	return _NEED_CONTROLLER;

## Тип контроллера тасков
var _TASK_CONTROLLER:int=CONTROLLERS.GOAP setget _TASK_CONTROLLER_SET, TASK_CONTROLLER_GET;
func _TASK_CONTROLLER_SET(value:int)->void:
	_TASK_CONTROLLER = value;
func TASK_CONTROLLER_GET()->int:
	return _TASK_CONTROLLER;

## Знания всех персонажей
var _knowledge:BitMap = BitMap.new();
func knowledge_get()->BitMap:
	return _knowledge;
