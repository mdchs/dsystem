extends GlobalObject

## Заявка на продажу предмета
class_name Bid

## Владелец заявки
var _owner:CharacterData setget _owner_set, owner_get;
func _owner_set(value:CharacterData)->void:
	_owner = value;
func owner_get()->CharacterData:
	return _owner;

## количество предметов
var _count:int setget _count_set, count_get;
func _count_set(value:int)->void:
	_count = value;
func count_get()->int:
	return _count;
func count_add(value:int)->void:
	self._count_set(_count+value)
func count_dec(value:int)->bool:
	if self.count_get() - value < 0:
		return false;
	self.count_add(-value);
	return true;

## стоймость штуки предмета в заявке
var _price:int setget price_set, price_get;
func price_set(value:int)->void:
	_price = value;
func price_get()->int:
	return _price;

func _init(owner:CharacterData=null,
		count:int=0,
		price:int=0)->void:
	self._owner_set(owner);
	self._count_set(count);
	self.price_set(price);

## перегрузка класса для пула объектов
func reload(
	owner:CharacterData,
	count:int,
	price:int)->void:
	self._init(owner,
				count,
				price);

## покупка предметов. Выдает остаток количества предметов в заявке
func buy(count:int)->int:
	count = int(min(self.count_get(), count));
	if !self.count_dec(count):
		return self.count_get();
	self.owner_get().money_add(count*self.price_get());
	return self.count_get();

static func bids_sort_max(
				a:Bid, 
				b:Bid)->bool:
	if a.price_get() > b.price_get():
		return true
	elif a > b:
		return true;
	elif b == null:
		return true;
	return false;
