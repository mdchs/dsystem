extends Reference

## Класс статистики Рынка
class_name StockExchangeStatistic

## количество предмета на рынке
const _counts:Dictionary = {};
static func _count_add(item:ItemData,
					count:int)->void:
	_counts[item] = count_get(item)+count;
static func _count_dec(item:ItemData,
					count:int)->void:
	_count_add(item, -count);
static func _sic_clear()->void:
	_counts.clear();
static func count_get(item:ItemData)->int:
	return _counts.get(item,0);
