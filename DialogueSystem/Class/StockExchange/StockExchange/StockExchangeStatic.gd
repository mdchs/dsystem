extends StockExchangeStatistic

## Класс Рынка для хранения констант и обработки заявок
class_name StockExchangeStatic

const _bids_pool:Array = [];
static func bid_pool_load(start_size:int)->void:
	for _i in range(0,start_size):
		_bid_pool_add(Bid.new());
static func _bid_pool_add(bid:Bid)->void:
	_bids_pool.append(bid);
static func _bid_pool_get(
				owner:CharacterData,
				count:int, 
				price:int)->Array:
	var bid:Bid = _bids_pool.pop_back();
	if !bid:
		return [Bid.new(
					owner,
					count, 
					price)];
	bid.reload(owner,
				count, 
				price)
	return [bid];

const _bids:Dictionary = {};
static func _bid_add(
			owner:CharacterData,
			item:ItemData, 
			count:int, 
			price:int)->void:
	_bids[item] = _bids_array_get(item)+_bid_pool_get(
							owner,
							count,
							price);
	._count_add(item, count);
	var min_price:int = min_price_get(item);
	if !min_price:
		min_price = price;
	item.price_set(min_price);
static func _bid_erase(item:ItemData,
						bid:Bid)->void:
	var arr:Array = _bids.get(item);
	if !arr.size():
		return;
	var pos:int = arr.find_last(bid);
	arr[pos] = null;
	._count_dec(item, bid.count_get());
	item.price_set(min_price_get(item));
	_bid_pool_add(bid);
static func _bids_array_get(item:ItemData)->Array:
	return _bids.get(item,[]);
static func _bid_get_by_min_price(item:ItemData)->Bid:
	var arr:Array = _bids_array_get(item);
	for bid_id in range(arr.size()-1,0,-1):
		if arr[bid_id] == null:
			continue;
		return arr[bid_id];
	return null;
static func _bid_close_payment(item:ItemData,
					bid:Bid,
					count:int)->void:
	var balance:int = bid.buy(count);
	if balance == 0:
		_bid_erase(item, bid);
	else:
		._count_dec(item, balance);
static func _bids_sort()->void:
	for arr in _bids.values():
		var elem_id:int = arr.size()-1;
		if elem_id < 0:
			continue;
		(arr as Array).sort_custom(Bid,"bids_sort_max");
		while arr[elem_id] == null:
			elem_id-=1;
			if elem_id <0:
				break;
		arr.resize(elem_id+1);

static func min_price_get(item:ItemData)->int:
	var bid:Bid = _bid_get_by_min_price(item);
	if !bid:
		return 0;
	return bid.price_get();
static func bid_add(
			owner:CharacterData,
			item:ItemData, 
			count:int, 
			price:int)->bool:
	if ItemStatic.is_money(item):
		return false;
	if !owner.inventory_get().dec(item, count):
		return false;
	if !price:
		return false;
	_bid_add(
		owner,
		item, 
		count, 
		price);
	return true;
static func bid_close(item:ItemData,
					bid:Bid)->void:
	_bid_erase(item, bid);
	bid.owner_get().inventory_get().add(item, bid.count_get());
static func bid_close_all_by_character(
					character:CharacterData,
					item:ItemData)->void:
	for bid in _bids_array_get(item):
		if bid.owner_get() == character:
			bid_close(item,bid);
static func buy(
			customer:CharacterData,
			item:ItemData,
			max_count:int)->bool:
	var money:int = customer.money_get();
	if !money:
		return false;
	var bids_arr:Array = _bids_array_get(item);
	var elem_id:int = bids_arr.size()-1;
	if elem_id < 0:
		return false;
	var scount:int=0;
	while scount < max_count:
		if elem_id <0:
			break;
		var bid:Bid = bids_arr[elem_id];
		## чтобы не покупали свои предметы
		if bid.owner_get() == customer:
			continue;
		var diff_cnt:int = int(1.0*money/bid.price_get());
		var nscount:int = int(min(max_count, scount+diff_cnt));
		money -= (nscount-scount)*bid.price_get();
		scount = nscount;
		_bid_close_payment(item,bid,nscount-scount);
		if money <=0:
			break;
		elem_id-=1;
	if scount:
		customer.inventory_get().add(item, scount);
		customer._money_set(money);
		return true;
	return false;
