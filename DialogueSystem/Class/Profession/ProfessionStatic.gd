extends Reference

## Статический класс для хранения констант
class_name ProfessionStatic

## Якоря профессийReceiptStatic
const _anchors_profession:Dictionary = {};
static func _anchor_add(anchor:String, value:ProfessionStatic)->void:
	_anchors_profession[anchor] = value;
static func anchor_get(anchor:String)->ProfessionStatic:
	return _anchors_profession.get(anchor);
