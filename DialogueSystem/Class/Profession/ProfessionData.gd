extends ProfessionStatic

## Класс с данными профессии
class_name ProfessionData

var _name:String setget _name_set, name_get;
func _name_set(value:String)->void:
	_name = value;
func name_get()->String:
	return _name;

var _receipts:Array setget , receipts_get;
func _receipts_set(value:Array)->void:
	_receipts = value;
func receipts_get()->Array:
	return _receipts;

func _init(name:String,
		receipts:Array,
		anchor:String = '')->void:
	self._name_set(name);
	self._receipts_set(receipts);
	._anchor_add(anchor, self);

static func profession_or_anchor_get(pobject)->ProfessionStatic:
	if (pobject is ProfessionStatic):
		return pobject;
	return anchor_get(str(pobject));

func work(receipt:Receipt,
		inventory:Inventory,
		count_work:int)->bool:
	if !self.receipts_get().has(receipt):
		return false;
	if !receipt.reagent_has(inventory, count_work):
		return false;
	for reag in receipt.reagents_get().keys():
		if !inventory.dec(reag, receipt.reagent_count_get(reag)*count_work):
			push_error('Что то пошло совсем не так');
	inventory.add(receipt.result_get(), count_work*receipt.result_count_get());
	return true;
