extends ConfigParser

## Класс парсинга конфига профессий
class_name ProfessionConfigParser

const _BASE_PROFESSIONS_PATH:String = 'res://Res/Configs/professions.ds';

const TAGS:Array = [
	'[NAME]',
	'[ANCHOR]',
	'[RECEIPTS]',
]

static func load_config()->bool:
	print('===Professions===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_PROFESSIONS_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'professions.ds file not found');
		return false;
	while file.position_get() < file.lenght():
		if !_parse(file):
			file.close();
			.print_err(file.line_position_get(),'professions.ds failed load ');
			return false;
	.print_err(file.line_position_get(),'professions.ds successfully load');
	file.close();
	return true;

static func _parse(file:FileWorker)->bool:
	var line_pos:int = file.line_position_get()+1;
	var params:PoolStringArray = ._tag_parse(file, TAGS);
	if !params.size():
		.print_err(line_pos,'Line is empty?');
		return false;
	if !params[0]:
		.print_err(line_pos,'Name is empty');
		return false;
	line_pos+=1;
	if params[1].length() < 2:
		.print_err(line_pos,'Anchor is empty');
		return false;
	if !params[1].begins_with('P'):
		.print_err(line_pos,'The anchor must start with P');
		return false;
	line_pos+=1;
	var receipts:Array = [];
	for ranchor in params[2].split(',',false):
		var receipt:Receipt = Receipt.anchor_get(ranchor);
		if !receipt:
			.print_err(line_pos,'Receipt anchor %s not found'%[ranchor]);
			return false;
		receipts.append(receipt);
# warning-ignore:return_value_discarded
	ProfessionData.new(params[0],
						receipts,
						params[1]);
	return true;
