extends ReceiptStatic

class_name Receipt

var _name:String setget _name_set, name_get;
func _name_set(value:String)->void:
	_name = value;
func name_get()->String:
	return _name;

var _result:ItemData setget _result_set, result_get;
func _result_set(value:ItemData)->void:
	_result = value;
func result_get()->ItemData:
	return _result;
func result_price_get()->int:
	return self.result_get()._quality_price_get()*self.lifetime_get();

var _result_count:int=1 setget _result_count_set, result_count_get;
func _result_count_set(value:int)->void:
	_result_count = value;
func result_count_get()->int:
	return _result_count;

var _reagents:Dictionary setget _reagents_set, reagents_get;
func _reagents_set(value:Dictionary)->void:
	_reagents = value;
func reagents_get()->Dictionary:
	return _reagents;
func reagent_count_get(item:ItemData)->int:
	return _reagents[item];
func reagent_has(inventory:Inventory,
				multiple:int = 1)->bool:
	for reag in self.reagents_get().keys():
		if inventory.count_get(reag) < self.reagent_count_get(reag)*multiple:
			return false;
	return true;
func spending_price()->int:
	var price:int = 0;
	for reag in self.reagents_get().keys():
		price += reag.price_get()*self.reagent_count_get(reag);
	return price;

var _lifetime:int=0 setget _lifetime_set, lifetime_get;
func _lifetime_set(value:int)->void:
	_lifetime = value;
func lifetime_get()->int:
	return _lifetime;
func result_price()->int:
	return self.spending_price()+self.result_price_get();
func result_price_via_receipt()->int:
# warning-ignore:integer_division
	return int(self.result_price_get()/self.result_count_get())

var _need_exp:int setget _need_exp_set, need_exp_get;
func _need_exp_set(value:int)->void:
	_need_exp = value;
func need_exp_get()->int:
	return _need_exp;
	
var _gain_exp:int setget _gain_exp_set, gain_exp_get;
func _gain_exp_set(value:int)->void:
	_gain_exp = value;
func gain_exp_get()->int:
	return _gain_exp;

static func receipt_or_anchor_get(robject)->ReceiptStatic:
	if (robject is ReceiptStatic):
		return robject;
	return anchor_get(robject);

func _init(name:String, 
			result:ItemData,
			result_count:int,
			need_exp:int,
			gain_exp:int,
			lifetime:int,
			reagents:Dictionary = {},
			anchor:String='')->void:
	self._name_set(name);
	self._result_set(result);
	self._result_count_set(result_count);
	self._need_exp_set(need_exp);
	self._gain_exp_set(gain_exp);
	self._reagents_set(reagents);
	self._lifetime_set(lifetime);
	._anchor_add(anchor, self);
