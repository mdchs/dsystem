extends ConfigParser

class_name ReceiptConfigParser

const _BASE_RECEIPTS_PATH:String = 'res://Res/Configs/receipts.ds';

const TAGS:Array = [
	'[NAME]',
	'[ANCHOR]',
	'[RESULT]',
	'[COUNT]',
	'[CRAFT_TIME]',
	'[REAGENTS]',
	'[NEED_EXP]',
	'[GAIN_EXP]',
]

static func load_config()->bool:
	print('===Receipts===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_RECEIPTS_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'receipts.ds file not found');
		return false;
	while file.position_get() < file.lenght():
# warning-ignore:return_value_discarded
		if !_parse(file):
			file.close();
			.print_err(file.line_position_get(),'receipts.ds failed load ');
			return false;
	.print_err(file.line_position_get(),'receipts.ds successfully load');
	file.close();
	return true;

static func _parse(file:FileWorker)->bool:
	var line_pos:int = file.line_position_get()+1;
	var params:PoolStringArray = ._tag_parse(file, TAGS);
	if !params.size():
		.print_err(line_pos,'Line is empty?');
		return false;
	if !params[0]:
		.print_err(line_pos,'[NAME] %s is empty'%[params[0]]);
		return false;
	line_pos+=1;
	if params[1].length() < 2:
		.print_err(line_pos+1,'[ANCHOR] %s is empty'%[params[1]]);
		return false;
	if !params[1].begins_with('R'):
		.print_err(line_pos,'[ANCHOR] %s must start with R'%[params[1]]);
		return false;
	line_pos+=1;
	var result:ItemData = ItemStatic.anchor_get(params[2]);
	if !result:
		.print_err(line_pos,'[RESULT] item anchor %s not found'%[params[2]]);
		return false;
	
	var result_count = 1;
	if params[3]:
		line_pos+=1;
		result_count = ._base_variable_convert(params[3]);
		if typeof(result_count) != TYPE_INT:
			.print_err(line_pos,'[COUNT] %s invalid type int'%[result_count]);
			return false;
		if result_count < 1:
			.print_err(line_pos,'[COUNT] %s < 1'%[result_count]);
			return false;
	
	var craft_time = 1;
	if params[4]:
		line_pos+=1;
		craft_time = ._base_variable_convert(params[4]);
		if typeof(craft_time) != TYPE_INT:
			.print_err(line_pos,'[CRAFT_TIME] %s invalid type int'%[craft_time]);
			return false;
		if craft_time < 1:
			.print_err(line_pos,'[CRAFT_TIME] %s < 1'%[craft_time]);
			return false;
	
	var reagents:Dictionary = {};
	if params[5]:
		line_pos+=1;
		reagents = _inventory_parse(line_pos,params[5]);
		if !reagents.size():
			return false;
	
	var need_exp = 0;
	if params[6]:
		line_pos+=1;
		need_exp = ._base_variable_convert(params[6])
		if typeof(need_exp) != TYPE_INT:
			.print_err(line_pos,'[NEED_EXP] %s invalid type int'%[need_exp]);
			return false;
		if need_exp < 0:
			.print_err(line_pos,'[NEED_EXP] %s < 0'%[need_exp]);
			return false;
	
	var gain_exp = 0;
	if params[7]:
		line_pos+=1;
		gain_exp = ._base_variable_convert(params[7]);
		if typeof(need_exp) != TYPE_INT:
			.print_err(line_pos,'[GAIN_EXP] %s invalid type int'%[need_exp]);
			return false;
		
# warning-ignore:return_value_discarded
	Receipt.new(
			params[0], 
			result,
			result_count,
			need_exp,
			gain_exp,
			craft_time,
			reagents,
			params[1]);
	return true;
