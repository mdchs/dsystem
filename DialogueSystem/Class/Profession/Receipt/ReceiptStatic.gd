extends Reference

class_name ReceiptStatic

const _anchors:Dictionary = {};
static func _anchor_add(anchor:String, value:ReceiptStatic)->void:
	_anchors[hash(anchor)] = value;
	_rm_add(value);
static func anchor_get(anchor:String)->ReceiptStatic:
	return _anchors.get(hash(anchor));

const _result_map:Dictionary = {};
static func _rm_add(receipt:ReceiptStatic)->void:
	_result_map[receipt.result_get()] = receipts_get(receipt.result_get())+[receipt];
static func receipts_get(item:ItemData)->Array:
	return _result_map.get(item, []);
static func receipt_get_by_min_price(item:ItemData)->ReceiptStatic:
	var arr:Array = receipts_get(item);
	if !arr.size():
		return null;
	var receipt = arr.front();
	for r_id in range(1,arr.size()):
		if receipt.result_price_via_receipt() < arr[r_id].result_price_via_receipt():
			receipt = arr[r_id];
	return receipt;
