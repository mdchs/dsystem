extends ConfigParser

## Класс парсер конфига сообщений
class_name MessageConfigParser

const _BASE_MESSAGES_PATH:String = 'res://Res/Configs/messages.ds';

enum {
	OK,
	ERR,
	NOT_CHILD
}

static func load_config()->bool:
	print('===Messages===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_MESSAGES_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'messages.ds file not found');
		return false;
	while file.position_get() < file.lenght():
		if _parse_message(file) == ERR:
			file.close();
			.print_err(file.line_position_get(),'messages.ds failed load ');
			return false;
	.print_err(file.line_position_get(),'messages.ds successfully load');
	file.close();
	return true;

static func _parse_message(file:FileWorker, 
						level:int = -1, 
						parent:MessageData = null)->int:
	var line:String = file.line_get();
	while line.begins_with('#') or !line.length():
		if file.position_get() < file.lenght():
			line = file.line_get();
		else:
			return NOT_CHILD;
	var last_tab_pos:int = ._find_last_seq_pos(line, '	');
	if parent:
		if last_tab_pos != level:
			file.move_start_line();
			return NOT_CHILD;
		line = line.substr(last_tab_pos+1);
	
	var line_pos:int = file.line_position_get();
	var anchor:String = '';
	if line.begins_with('M'):
		var pos:int = line.find(':',1);
		if pos > 1:
			anchor = line.substr(0,pos);
			line = line.substr(pos+1);
	if !anchor:
		.print_err(line_pos,'Anchor %s not found! Missing colon?'%[anchor]);
		return ERR;
	
	if line.begins_with('>'):
		line = line.substr(1);
		var cmessage:MessageData = MessageStatic.anchor_get(anchor);
		if !cmessage:
			.print_err(line_pos,'Child message not found!');
			return ERR;
		for manchor in line.split(';',false):
			var pmessage = MessageStatic.anchor_get(manchor);
			if !pmessage:
				.print_err(line_pos,'Parent message not found!');
				return ERR;
			pmessage._child_add(cmessage);
		return OK;
	var is_choise:bool = false;
	if line.begins_with('@p:'):
		is_choise = true;
	elif !line.begins_with('@i:'):
		.print_err(line_pos,'Message autor not found!');
		return ERR;
	line = line.substr(3);
	
	var condition:ConditionUnarBit;
	var actions:Array = [];
	var pos:int = line.find('do:');
	if pos >=0:
		actions = FunctionConfigParser.parse_function_list(line_pos,line.substr(pos+3));
		if !actions.size():
			.print_err(line_pos,'Actions %s not create!'%[line.substr(pos+3)]);
			return ERR;
		line = line.substr(0,pos);
	pos = line.find('if:');
	if pos >= 0:
		condition = ConditionConfigParser.parse_condition(line_pos,line.substr(pos+3));
		if !condition:
			.print_err(line_pos,'Condition %s not create!'%[line.substr(pos+3)]);
			return ERR;
		line = line.substr(0, pos);
	var text_array:Array = _parse_text(line_pos,line.substr(0,pos));
	if !text_array.size():
		return ERR;
	## зачем хранить массив из одной пустой строки.
	if text_array.size() == 1 and !text_array.front().length():
		text_array = [];
	var cmessage:MessageData = MessageData.new(
										text_array,
										condition,
										[],
										actions,
										is_choise,
										anchor);
	if parent:
		parent._child_add(cmessage);
	level+=1;
	var result:int =  _parse_message(file, level, cmessage);
	while result == OK:
		result = _parse_message(file, level, cmessage);
	if result == ERR:
		return ERR;
	return OK;

static func _parse_text(line:int, value:String)->Array:
	value = ._clear_char(value,' ');
	var arr:Array = [];
	var crr_pos:int = 0;
	for reg in Config.FUNCTION_REGEX.search_all(value):
		var regmatch:RegExMatch = (reg as RegExMatch);
		var pos:int = regmatch.get_start();
		if crr_pos != pos:
			arr.append(value.substr(crr_pos, pos-crr_pos));
		var result:String = regmatch.get_string();
		result = result.substr(1, result.length()-2);
		crr_pos = regmatch.get_end();
		var fnc:FunctionAnchor = FunctionConfigParser.parse_function(line,result);
		if !fnc:
			.print_err(line,'Function %s not create!'%[result]);
			return [];
		arr.append(fnc);
	arr.append(value.substr(crr_pos));
	return arr;
