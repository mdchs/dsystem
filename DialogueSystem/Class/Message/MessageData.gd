extends MessageStatic

## Класс данных сообщения
class_name MessageData

var _text:Array;
func text_set(value:Array)->void:
	_text = value;
func text_get()->String:
	var text:String = '';
	for elem in self._text:
		if (elem is FunctionData):
			text += str(elem.exec());
		else:
			text +=elem;
	return text;
func is_empty()->bool:
	return _text.empty();

var _is_choise:bool=false setget is_choise_set, is_choise_get;
func is_choise_set(value:bool)->void:
	_is_choise = value;
func is_choise_get()->bool:
	return _is_choise;

var _condition:ConditionUnarBit setget _condition_set, condition_get;
func _condition_set(value:ConditionUnarBit)->void:
	_condition = value;
func condition_get()->ConditionUnarBit:
	return _condition;
func is_check()->bool:
	if !self.condition_get():
		return true;
	return self.condition_get().exec();

var _childs:Array setget ,childs_get;
func _child_add(value:MessageData)->void:
	_childs.append(value);
func childs_get()->Array:
	return _childs;

var _actions:Array setget _actions_set,actions_get;
func _actions_set(value:Array)->void:
	_actions = value;
func actions_get()->Array:
	return _actions;

static func message_or_anchor_get(mobject)->MessageStatic:
	if (mobject is MessageStatic):
		return mobject;
	return anchor_get(str(mobject));

func _init(text:Array,
			condition:ConditionUnarBit, 
			childs:Array,
			actions:Array,
			is_choise:bool,
			anchor:String = '')->void:
	self.text_set(text);
	self._actions_set(actions);
	for child in childs:
		self._child_add(child);
	self._condition_set(condition);
	self.is_choise_set(is_choise);
	if anchor:
		self._anchor_add(anchor,self);
