extends Reference

## Статический класс для хранения информации о сообщениях
class_name MessageStatic

## Словарь Якорей Сообщений
const _anchors_messages:Dictionary = {};
static func _anchor_add(anchor:String, value:MessageStatic)->void:
	_anchors_messages[anchor] = value;
static func anchor_get(anchor:String)->MessageStatic:
	return _anchors_messages.get(anchor);
