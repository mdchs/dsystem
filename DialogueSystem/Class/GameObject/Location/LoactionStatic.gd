extends GameObject

## Статический класс для хранения данных
class_name LocationStatic

## Якори Локаций для обработки кода из Конфигов
const _anchors_location:Dictionary = {};
static func _anchor_add(anchor:String, value:LocationStatic)->void:
	_anchors_location[hash(anchor)] = value;
static func anchor_get(anchor:String)->LocationStatic:
	return _anchors_location.get(hash(anchor));

static func player_position_set(value:LocationStatic)->void:
	_anchor_add(Config.PLAYER_POSITION, value);
static func player_postition_get()->LocationStatic:
	return anchor_get(Config.PLAYER_POSITION);
