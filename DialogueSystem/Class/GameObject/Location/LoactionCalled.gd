extends LocationLogic

## Класс интерфейс для взаимодействия с LocationLogic из конфига
class_name LocationCalled

func _init(
		name:String, 
		file_name:String, 
		coord:Vector2,
		points:Array,
		anchor:String
		).(
		name, 
		file_name, 
		coord,
		points,
		anchor
		)->void:
	pass;

####################################################
###                   БАЗА                       ###
####################################################
## Получить имя предмета
func name()->String:
	return self.name_get();
