extends LocationData

## Класс для сложной логики с LocationData
class_name LocationLogic

func _init(
		name:String, 
		file_name:String, 
		coord:Vector2,
		points:Array,
		anchor:String
		).(
		name, 
		file_name, 
		coord,
		points,
		anchor
		)->void:
	pass;

####################################################
###                   БАЗА                       ###
####################################################
## Получить имя локации
func name()->String:
	return self.name_get();
