extends Reference

class_name Point

var _position:Vector2 setget _position_set, position_get;
func _position_set(value:Vector2)->void:
	_position = value;
func position_get()->Vector2:
	return _position;

var _scale:float setget _scale_set, scale_get;
func _scale_set(value:float)->void:
	_scale = value;
func scale_get()->float:
	return _scale;

var _stuff:StuffData setget _stuff_set, stuff_get;
func _stuff_set(value:StuffData)->void:
	_stuff = value;
func stuff_get()->StuffData:
	return _stuff;

func is_engaged()->bool:
	if !self.stuff_get():
		return false;
	return self.stuff_get().is_engaged_get();

func _init(position:Vector2, scale:float, stuff:StuffData)->void:
	self._position_set(position);
	self._scale_set(scale);
	self._stuff_set(stuff);
