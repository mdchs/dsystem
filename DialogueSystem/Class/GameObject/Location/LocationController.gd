extends Reference

## Обработчик действия с локациями
class_name LocationContoller

const _MAX_STEP_FIND:int = 250;
static func path_get(location_start:LocationData,
					location_end:LocationData)->Dictionary:
	if location_start == location_end:
		return {};
	var open:Array = [];
	var close:Array = [];
	open.append(_P.new(location_start));
	var step:int = 0;
	var current:_P;
	var temp_array:Array = [];
	while step < _MAX_STEP_FIND +1:
		current = open.pop_back();
		if current.location_get() == location_end:
			break;
		step+=1;
		var locations:Array = current.location_get().transits_get().keys();
		for location in locations:
			if _P.location_array_has(location, open):
				continue;
			if _P.location_array_has(location, close):
				continue;
			temp_array.append(_P.new(location,locations[location],current));
		close.append(current);
		if temp_array.size():
			open = open + temp_array;
			temp_array.clear();
		if open.empty():
			return {};
		open.sort_custom(_P,"sort_max");
	if step > _MAX_STEP_FIND:
		return {};
	var return_array:Array = [current.location_get()];
	var pos:_P = current.father_get();
	while pos.father_get() != null:
		step -=1;
		if step < 0:
			return {};
		return_array.append(pos.location_get());
		pos = pos.father_get();
	return_array.invert();
	return {'path':return_array, 
			'weight':current.weight_get()};

class _P:
	var _location:LocationData;
	func location_get()->LocationData:
		return _location;
	func distance_get()->float:
		return _location.coord_get().distance_to(_father.location_get().coord_get());
	var _father:_P = null;
	func father_get()->_P:
		return _father;
	var _weight:int = 0;
	func weight_get()->int:
		return _weight;
	func _init(location:LocationData,
				weight:int=0,
				father:_P=null)->void:
		_location = location;
		if !father:
			return;
		_father = father;
		_weight = int(weight*self.distance_get() + father.weight_get());
	static func sort_max(a:_P, b:_P):
		if a.weight_get() > b.weight_get():
			return true
		return false
	static func location_array_has(location:LocationData, array:Array)->bool:
		for elem in array:
			if elem.location_get() == location:
				return true;
		return false;
