extends ConfigParser

## Класс парсер конфига Локаций
class_name LocationConfigParser

const _BASE_LOCATIONS_PATH:String = 'res://Res/Configs/locations.ds';

const TAGS:Array = [
	'[NAME]',
	'[ANCHOR]',
	'[IMAGE]',
	'[COORD]',
	'[TRANSITS]',
	'[POINTS]',
	'[STUFFS]'
]

static func load_config()->bool:
	print('===Locations===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_LOCATIONS_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'locations.ds file not found');
		return false;
	var portals:Array = [];
	while file.position_get() < file.lenght():
		if !_parse(file,portals):
			file.close();
			.print_err(file.line_position_get(),'locations.ds failed load');
			return false;
	file.close();
	if !_portal_parse(portals):
		.print_err(file.line_position_get(),'locations.ds failed load');
		return false;
	.print_err(file.line_position_get(),'locations.ds successfully load');
	return true;

static func _parse(file:FileWorker, portals:Array)->bool:
	var line_pos:int = file.line_position_get()+1;
	var params:PoolStringArray = ._tag_parse(file, TAGS);
	if !params.size():
		.print_err(line_pos,'Line is empty?');
		return false;
	if !params[0]:
		.print_err(line_pos,'[NAME] %s is empty'%[params[0]]);
		return false;
	line_pos+=1;
	if params[1].length() < 2:
		.print_err(line_pos,'[ANCHOR] %s is empty'%[params[1]]);
		return false;
	elif !params[1].begins_with('#'):
		.print_err(line_pos,'[ANCHOR] %s must start with #'%[params[1]]);
		return false;
	elif ['#p'].has(params[1]):
		.print_err(line_pos,'[ANCHOR] %s cannot be named #p'%[params[1]]);
		return false;
	line_pos+=1;
	if params[2].length()<6:
		.print_err(line_pos,'[IMAGE] %s is empty'%[params[2]]);
		return false;
	elif !params[2].ends_with('.png'):
		.print_err(line_pos,'[IMAGE] %s are not in PNG format'%[params[2]]);
		return false;
	elif !Config.DEFAULT_DIRECTORY.file_exists(Config.LOCATION_IMAGE_PATH+params[2]):
		.print_err(line_pos,'[IMAGE] not found: %s'%[Config.LOCATION_IMAGE_PATH+params[2]]);
		return false;
	elif !Config.DEFAULT_DIRECTORY.file_exists(Config.LOCATION_IMAGE_PATH+'L'+params[2]):
		.print_err(line_pos,'[IMAGE] Light not found: %s'%[Config.LOCATION_IMAGE_PATH+'L'+params[2]]);
		return false;
	line_pos+=1;
	var coord:Vector2 = _coord_parse(line_pos,params[3]);
	if coord == Vector2.INF:
		return false;
	if params[4]:
		line_pos+=1;
		var location_portals:Array = _portals_parse(line_pos,params[4], params[1]);
		if !location_portals.size():
			return false;
		portals.append_array(location_portals);
	var points:Array = [];
	if params[5].length():
		line_pos+=1;
		points = _points_parse(line_pos, params[5]);
		if !points.size():
			return false;
	var stuffs:Array = [];
	if params[6].length():
		line_pos+=1;
		stuffs = _stuffs_parse(line_pos, params[6]);
		if !stuffs.size():
			return false;
# warning-ignore:return_value_discarded
	LocationCalled.new(params[0],
					params[2],
					coord,
					_points_concat_stuffs(
								line_pos,
								points, 
								stuffs),
					params[1]);
	return true;

static func _portal_parse(portals:Array)->bool:
	for portal_data in portals:
# warning-ignore:return_value_discarded
		var location1:LocationCalled = LocationStatic.anchor_get(portal_data[0]);
		var location2:LocationCalled = LocationStatic.anchor_get(portal_data[1]);
		if !location2:
			print('Location Anchor %s not found'%[portal_data[1]]);
			return false;
		location1.transit_set(location2, int(portal_data[2]));
		location2.transit_set(location1, int(portal_data[2]));
	return true;
