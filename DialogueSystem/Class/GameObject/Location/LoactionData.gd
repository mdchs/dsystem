extends LocationStatic

## Класс для хранения данных по локации
class_name LocationData

# warning-ignore:unused_signal
signal LocationEnter(character)

var _name:String setget _name_set, name_get;
func _name_set(value:String)->void:
	_name = value;
func name_get()->String:
	return _name;

var _image:String setget _image_set, image_get;
func _image_set(value:String)->void:
	_image = value;
func image_get()->String:
	return _image;

func characters_get()->Array:
	return CharacterStatic.location_characters_get(self);

var _coord:Vector2 setget _coord_set, coord_get;
func _coord_set(value:Vector2)->void:
	_coord = value;
func coord_get()->Vector2:
	return _coord;

var _transits:Dictionary setget _transits_set, transits_get;
func _transits_set(value:Dictionary)->void:
	_transits = value;
func transits_get()->Dictionary:
	return _transits;
func transit_set(value:LocationData, weight:int)->void:
	_transits[value] = weight
func transit_weight_get(value:LocationData)->int:
	return _transits.get(value,0);
func transit_time_get(value:LocationData)->int:
	return int(self.coord_get().distance_to(value.coord_get())*transit_weight_get(value));

var _points:Array setget _points_set, points_get;
func _points_set(value:Array)->void:
	_points = value;
func points_get()->Array:
	return _points;

static func location_or_anchor_get(lobject)->LocationStatic:
	if (lobject is LocationStatic):
		return lobject;
	return anchor_get(str(lobject));

func _init(name:String, 
		image_name:String, 
		coord:Vector2,
		points:Array,
		anchor:String)->void:
	self._name_set(name);
	self._image_set(image_name);
	self._coord_set(coord);
	self._points_set(points);
	self._anchor_add(anchor,self);
