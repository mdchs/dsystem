extends StuffStatic

class_name StuffData

var _name:String setget _name_set, name_get;
func _name_set(value:String)->void:
	_name = value;
func name_get()->String:
	return _name;

var _image:String setget _image_set, image_get;
func _image_set(value:String)->void:
	_image = value;
func image_get()->String:
	return _image;

var _animation:String setget _animation_set, animation_get;
func _animation_set(value:String)->void:
	_animation = value;
func animation_get()->String:
	return _animation;

var _is_interaction:bool setget _is_interaction_set, is_interaction_get;
func _is_interaction_set(value:bool)->void:
	_is_interaction = value;
func is_interaction_get()->bool:
	return _is_interaction;

var _is_engaged:bool setget _is_engaged_set, is_engaged_get;
func _is_engaged_set(value:bool)->void:
	_is_engaged = value;
func is_engaged_get()->bool:
	return _is_engaged;

var _actions:Array setget _actions_set,actions_get;
func _actions_set(value:Array)->void:
	_actions = value;
func actions_get()->Array:
	return _actions;
func actions_exec(u:GameObject)->void:
	for action in self.actions_get():
		if action._anchor_get() == '@u':
			action.exec(u);
		else:
			action.exec();

static func stuff_or_anchor_get(sobject)->StuffStatic:
	if (sobject is StuffStatic):
		return sobject;
	return anchor_get(str(sobject));

func _init(
		name:String,
		image:String,
		animation:String,
		is_interaction:bool,
		is_engaged:bool,
		actions:Array,
		anchor:String)->void:
	self._name_set(name);
	self._image_set(image);
	self._animation_set(animation);
	self._is_interaction_set(is_interaction);
	self._is_engaged_set(is_engaged);
	self._actions_set(actions);
	self._anchor_add(anchor,self);
