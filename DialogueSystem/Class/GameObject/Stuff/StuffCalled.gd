extends StuffLogic

## Класс-интерфейс для работы с ВЕЩАМИ из конфига
class_name StuffCalled

func _init(
		name:String,
		image:String,
		animation:String,
		is_interaction:bool,
		is_engaged:bool,
		actions:Array,
		anchor:String
		).(
		name,
		image,
		animation,
		is_interaction,
		is_engaged,
		actions,
		anchor
		)->void:
	pass;

####################################################
###                   БАЗА                       ###
####################################################
## Получить имя ВЕЩИ
func name()->String:
	return self.name_get();
