extends GameObject

## Класс для хранения статических конструкций
class_name StuffStatic

## хранение якорей вещей
const _anchors_stuffs:Dictionary = {};
static func _anchor_add(anchor:String, value:StuffStatic)->void:
	_anchors_stuffs[hash(anchor)] = value;
static func anchor_get(anchor:String)->StuffStatic:
	return _anchors_stuffs.get(hash(anchor));
