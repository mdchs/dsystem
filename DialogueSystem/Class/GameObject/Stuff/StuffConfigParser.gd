extends ConfigParser

## Класс парсер конфига Вещей
class_name StuffConfigParser

const _BASE_ITEMS_PATH:String = 'res://Res/Configs/stuffs.ds';

const TAGS:Array = [
	'[NAME]',
	'[ANCHOR]',
	'[IMAGE]',
	'[ANIMATION]',
	'[INTERACTION]',
	'[ENGAGED]',
	'[USAGE_CALL]',
]

static func load_config()->bool:
	print('===Stuffs===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_ITEMS_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'stuffs.ds file not found');
		return false;
	while file.position_get() < file.lenght():
# warning-ignore:return_value_discarded
		if !_parse(file):
			file.close();
			.print_err(file.line_position_get(),'stuffs.ds failed load ');
			return false;
	.print_err(file.line_position_get(),'stuffs.ds successfully load');
	file.close();
	return true;

static func _parse(file:FileWorker)->bool:
	var line_pos:int = file.line_position_get()+1;
	var params:PoolStringArray = ._tag_parse(file, TAGS);
	if !params.size():
		.print_err(line_pos,'Line is empty?');
		return false;
	if !params[0]:
		.print_err(line_pos,'[NAME] %s is empty'%[params[0]]);
		return false;
	line_pos+=1;
	if params[1].length() < 2:
		.print_err(line_pos,'[ANCHOR] %s is empty'%[params[1]]);
		return false;
	if !params[1].begins_with('%'):
		.print_err(line_pos,'[ANCHOR] %s must start with %'%[params[1]]);
		return false;
	line_pos+=1;
	if params[2].length()<6:
		.print_err(line_pos,'[IMAGE] %s is empty'%[params[2]]);
		return false;
	if !params[2].ends_with('.png'):
		.print_err(line_pos,'[IMAGE] %s are not in PNG format'%[params[2]]);
		return false;
	var path:String = Config.STUFF_IMAGE_PATH+params[2];
	if !Config.DEFAULT_DIRECTORY.file_exists(path):
		.print_err(line_pos,'[IMAGE] not found: %s'%[path]);
		return false;
	line_pos+=1;
	var is_interaction = false;
	if params[4]:
		line_pos+=1;
		is_interaction = ._base_variable_convert(params[4]);
		if typeof(is_interaction) != TYPE_BOOL:
			.print_err(line_pos,'[INTERACTION] not boolean type: %s'%[is_interaction]);
			return false;
	var is_engaged = false;
	if params[5]:
		line_pos+=1;
		is_engaged = ._base_variable_convert(params[5]);
		if typeof(is_engaged) != TYPE_BOOL:
			.print_err(line_pos,'[ENGAGED] not boolean type: %s'%[is_engaged]);
			return false;
	var actions:Array = [];
	if params[6]:
		line_pos+=1;
		actions = FunctionConfigParser.parse_function_list(line_pos, params[6]);
		if !actions:
			.print_err(line_pos,'[USAGE_CALL] %s not create!'%[params[6]]);
			return false;
# warning-ignore:return_value_discarded
	StuffCalled.new(
				params[0],
				params[2],
				params[3],
				bool(is_interaction),
				bool(is_engaged),
				actions,
				params[1]
				);
	return true;
