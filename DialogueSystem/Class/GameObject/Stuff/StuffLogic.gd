extends StuffData

## Класс для сложной логики по Вещам
class_name StuffLogic

func _init(
		name:String,
		image:String,
		animation:String,
		is_interaction:bool,
		is_engaged:bool,
		actions:Array,
		anchor:String
		).(
		name,
		image,
		animation,
		is_interaction,
		is_engaged,
		actions,
		anchor
		)->void:
	pass;


