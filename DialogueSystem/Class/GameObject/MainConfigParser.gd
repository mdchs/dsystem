extends ConfigParser

## парсер конфига устанавливающего начальные параметры мира
class_name MainConfigParser

const _BASE_MAIN_PATH:String = 'res://Res/Configs/main.ds';

const TAGS:Array = [
	'[START_ACTIONS]',
	'[NEEDS_CONTROLLER]',
	'[TASKS_CONTROLLER]',
]

## основная функция конфига
static func load_config()->bool:
	print('===Main===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_MAIN_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'main.ds file not found');
		return false;
	while file.position_get() < file.lenght():
# warning-ignore:return_value_discarded
		if !_parse(file):
			.print_err(file.line_position_get(),'main.ds failed load ');
			file.close();
			return false;
	.print_err(file.line_position_get(),'main.ds successfully load');
	file.close();
	return true;

## разбор персонажа по тегам и преобразования текста в структуры данныъ
static func _parse(file:FileWorker)->bool:
	var line_pos:int = file.line_position_get()+1;
	var params:PoolStringArray = ._tag_parse(file, TAGS);
	if !params.size():
		.print_err(line_pos,'Line is empty?');
		return false;
	if params[0]:
		var actions:Array = FunctionConfigParser.parse_function_list(line_pos,params[0]);
		if !actions.size():
			return false;
		Config.START_ACTIONS_SET(actions);
		line_pos+=1;
	if params[1]:
		params[1] = ._clear_char(params[1], ' ');
		match params[1]:
			'NONE': Config._NEED_CONTROLLER_SET(Config.CONTROLLERS.NONE);
			'SOUL': Config._NEED_CONTROLLER_SET(Config.CONTROLLERS.SOUL);
			_:
				.print_err(line_pos,'[NEEDS_CONTROLLER] %s not found'%params[1]);
				return false;
		line_pos+=1;
	if params[2]:
		params[2] = ._clear_char(params[2], ' ');
		match params[2]:
			'NONE': Config._TASK_CONTROLLER_SET(Config.CONTROLLERS.NONE);
			'GOAP': Config._TASK_CONTROLLER_SET(Config.CONTROLLERS.GOAP);
			_:
				.print_err(line_pos,'[TASKS_CONTROLLER] %s not found'%params[2]);
				return false;
		line_pos+=1;
	return true;
