extends GlobalObject

## Класс для вызовов глобальных переменных из конфига
class_name GlobalObjectCalled

####################################################
###                   БАЗА                       ###
####################################################
##получить текущее время (минуты)
static func time()->int:
	return .time_get();
##получить текущий день
static func day()->int:
	return .day();
## закончить диалог
static func end()->void:
	.dialogue_get().dialogue_end();
## пропустить время
static func wait(time:int)->void:
	time_inc(time);
## скрыть/показать интерфейс
static func ivis(value:bool)->void:
	.dashboard_get().has_show_set(value);
## убрать/дать возможность интеракции с предметами 
## на время присутсвия в локации
static func pist(value:bool)->void:
	.place_get().IS_INTERACTION_SET_STUFF(value);
##открыть панель с текстовым сообщением
static func rdbs(nme:String, text:String)->void:
	.readboard_get().readboard_show(nme, text);
## Задать переменную
static func vset(name:String, variable)->void:
	if typeof(variable) == TYPE_STRING:
		var new_variable;
		match variable[0]:
			'@': new_variable = CharacterStatic.anchor_get(variable);
			'#': new_variable = LocationStatic.anchor_get(variable);
			'&': new_variable = ItemStatic.anchor_get(variable);
			'%': new_variable = StuffStatic.anchor_get(variable);
			'M': new_variable = MessageStatic.anchor_get(variable);
		if new_variable:
			variable = new_variable;
	.vset(name, variable);
## получить переменную
static func vget(name:String):
	return .vget(name);
