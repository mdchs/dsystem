extends ConfigParser

## Класс парсер конфига Предметов
class_name ItemConfigParser

const _BASE_ITEMS_PATH:String = 'res://Res/Configs/items.ds';

const TAGS:Array = [
	'[NAME]',
	'[ANCHOR]',
	'[IMAGE]',
	'[QUALITIES]',
	'[TYPE]',
	'[USAGE_CALL]',
]

static func load_config()->bool:
	print('===Items===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_ITEMS_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'items.ds file not found');
		return false;
	while file.position_get() < file.lenght():
# warning-ignore:return_value_discarded
		if !_parse(file):
			file.close();
			.print_err(file.line_position_get(),'items.ds failed load');
			return false;
	if !ItemStatic.money_item_get():
		.print_err(file.line_position_get(),'[ANCHOR] &m no found in items.');
		return false;
	.print_err(file.line_position_get(),'items.ds successfully load');
	file.close();
	return true;

static func _parse(file:FileWorker)->bool:
	var line_pos:int = file.line_position_get()+1;
	var params:PoolStringArray = ._tag_parse(file, TAGS);
	if !params.size():
		.print_err(line_pos,'Line is empty?');
		return false;
	if !params[0]:
		.print_err(line_pos,'[NAME] %s is empty'%[params[0]]);
		return false;
	line_pos+=1;
	if params[1].length() < 2:
		.print_err(line_pos,'[ANCHOR] %s is empty'%[params[1]]);
		return false;
	if !params[1].begins_with('&'):
		.print_err(line_pos,'[ANCHOR] %s must start with &'%[params[1]]);
		return false;
	line_pos+=1;
	if !params[2]:
		.print_err(line_pos,'[IMAGE] %s is empty'%[params[2]]);
		return false;
	var image_coord:Vector2 = _coord_parse(line_pos, params[2]);
	if image_coord == Vector2.INF:
		return false;
	line_pos+=1;
	var qualities:PoolIntArray = ._quality_parse(line_pos, params[3]);
	if !qualities.size():
		return false;
	var item_type_id:int = 0;
	if params[4]:
		line_pos+=1;
		item_type_id = ItemStatic._item_type_name_indx_get(params[4]);
	var actions:Array = [];
	if params[5]:
		line_pos+=1;
		actions = FunctionConfigParser.parse_function_list(line_pos, params[5]);
		if !actions:
			.print_err(line_pos,'[USAGE_CALL] %s not create!'%[params[5]]);
			return false;
# warning-ignore:return_value_discarded
	ItemCalled.new(params[0], 
				image_coord,
				qualities,
				item_type_id,
				actions,
				params[1]);
	return true;
