extends ItemStatic

## Класс данных Предмета
class_name ItemData

var _name:String setget _name_set, name_get;
func _name_set(value:String)->void:
	_name = value;
func name_get()->String:
	return _name;

var _image:Vector2 setget _image_set, image_get;
func _image_set(value:Vector2)->void:
	_image = value;
func image_get()->Vector2:
	return _image;

var _qualities:Qualities setget _qualities_set,qualities_get;
func _qualities_set(value:Qualities)->void:
	_qualities = value;
func qualities_get()->Qualities:
	return _qualities;
var _quality_price:int=0;
func _quality_price_calc()->void:
	for q in _qualities.get_all():
		_quality_price += .factorial(q);
func _quality_price_get()->int:
	return _quality_price;

var _price:int=0 setget price_set, price_get;
func price_set(value:int)->void:
	if ItemStatic.is_money(self):
		_price = value;
	else:
		_price = int(value/ItemStatic.money_item_get().price_get());
func price_get()->int:
	return _price;

static func item_or_anchor_get(iobject)->ItemStatic:
	if (iobject is ItemStatic):
		return iobject;
	return anchor_get(str(iobject));

var _item_type_id:int=0 setget _item_type_id_set, item_type_id_get;
func _item_type_id_set(value:int)->void:
	_item_type_id = value;
func item_type_id_get()->int:
	return _item_type_id;

var _actions:Array setget _actions_set,actions_get;
func _actions_set(value:Array)->void:
	_actions = value;
func actions_get()->Array:
	return _actions;
func actions_exec(u:GameObject)->void:
	for action in self.actions_get():
		if action._anchor_get() == '@u':
			action.exec(u);
		else:
			action.exec();

func _init(name:String, 
		image:Vector2,
		qualities:PoolIntArray,
		item_type_id:int,
		actions:Array,
		anchor:String='')->void:
	self._name_set(name);
	self._image_set(image);
	self._item_type_id_set(item_type_id);
	self._actions_set(actions);
	self._qualities_set(Qualities.new(qualities));
	self._quality_price_calc();
	self._anchor_add(anchor, self);
