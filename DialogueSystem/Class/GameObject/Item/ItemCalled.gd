extends ItemLogic

## Класс-интерфейс для вызова функций из конфига
class_name ItemCalled

func _init(name:String, 
		image:Vector2,
		qualities:PoolIntArray,
		item_type_id:int,
		actions:Array,
		anchor:String=''
		).(
		name, 
		image,
		qualities,
		item_type_id,
		actions,
		anchor
		)->void:
	pass;

####################################################
###                   БАЗА                       ###
####################################################
## Получить имя предмета
func name()->String:
	return self.name_get();
## Получить минимальную стоимость предмета
func price()->int:
	return self.price_get();
## Получить количество требуемое персонажам
func tacnt()->int:
	return self.task_count_get();
## Получить количество в продаже
func trcnt()->int:
	return self.trade_count_get();
## Получить наличие в продаже в виде текста
func tcnts()->String:
	if self.trcnt():
		return 'В продаже';
	return 'Отсутсвует в продаже';
