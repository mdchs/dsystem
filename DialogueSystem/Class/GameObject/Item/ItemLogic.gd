extends ItemData

## Класс обработки сложной логики с помощью данных ItemData
class_name ItemLogic

const _SURPLUS_PER_UNIT:float = 0.01;

func _init(name:String, 
		image:Vector2,
		qualities:PoolIntArray,
		item_type_id:int,
		actions:Array,
		anchor:String=''
		).(
		name, 
		image,
		qualities,
		item_type_id,
		actions,
		anchor
		)->void:
	pass;
## взять стоимость предмета
func price_get()->int:
	if !.price_get():
		self.price_set(self._price_via_receipt_get());
	return .price_get();
## стоимость предмета по стоимости рецепта
func _price_via_receipt_get()->int:
	var receipt:Receipt = ReceiptStatic.receipt_get_by_min_price(self);
	if receipt:
		return receipt.result_price_via_receipt();
	return int(1.0*self._quality_price_get()/receipt.result_count_get())*100;
## количество предметов в продаже
func trade_count_get()->int:
	return StockExchangeStatistic.count_get(self);
## количество предметов как потребность
func task_count_get()->int:
	return NeedStatic._quality_count_get(
				self.qualities_get().need_max_get_get(),
				self.qualities_get().max_get());
## прибавка или снижение стоимости предмета 
## в зависимости от разницы в количестве предложения и спроса
func demand_coef_get()->float:
	var task_count:int = self.task_count_get();
	var trade_count:int = self.trade_count_get();
	return (task_count-trade_count)*_SURPLUS_PER_UNIT;
