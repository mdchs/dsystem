extends GameObject

## Статический класс для хранения статических конструкций
class_name ItemStatic

## Хранение якорей предметов
const _anchors:Dictionary = {};
static func _anchor_add(anchor:String, value:ItemStatic)->void:
	_anchors[hash(anchor)] = value;
	_item_quality_add(value);
static func anchor_get(anchor:String)->ItemStatic:
	return _anchors.get(hash(anchor));
static func money_item_get()->ItemStatic:
	return _anchors.get(hash(Config.MONEY_ANCHOR));
static func is_money(item:ItemStatic)->bool:
	return item == money_item_get();

## Хранение предметов относительно их нужд+качества
const _items_quality:Dictionary = {};
static func _item_quality_add(item:ItemStatic)->void:
	for need_id in item.qualities_get().size():
		var quality:int = item.qualities_get().get_by_need(need_id);
		if !quality:
			continue;
		_items_quality[_path(need_id,quality)] = items_get_by_quality(need_id, quality)+[item];
static func items_get_by_quality(need_id:int,
								quality:int)->Array:
	return _items_quality.get(_path(need_id,quality),[]);

## в срочном порядке переписать на нормальный массив со сменой чиселок
## хотя сейчас там один элемент и похуй
static func item_get_by_quality_min_price(
								need_id:int,
								quality:int):
	var arr:Array = items_get_by_quality(need_id, quality);
	if !arr.size():
		return null;
	var mp_item = arr.front();
	for item_id in range(1, arr.size()):
		if mp_item.price_get() > arr[item_id].price_get():
			mp_item = arr[item_id];
	return mp_item;

static func _path(need_id:int, quality:int)->int:
	return hash('%s_%s'%[need_id,quality]);
	
const _item_type_names:Dictionary = {};
static func _item_type_name_indx_get(name:String)->int:
	var namehash:int = hash(name);
	var size:int = _item_type_names.get(namehash,-1);
	if size < 0:
		size = _item_type_names.size();
		_item_type_names[namehash] = size;
	return size;
