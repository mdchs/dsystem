extends Reference

## Класс объединяющий все классы использующие глобальные переменные
class_name GlobalObject

const _g_params:Dictionary = {
	'TIME':0, ## Игровое время в минутах
	'DIALOGUE':null, ## Нода диалога
	'DASHBOARD':null, ## Интерфейс 
	'PLACE':null, ## текущая визуальная!! локация
	'READBOARD':null, ##для текстовых сообщений
}
static func time_get()->int:
	return _g_params.get('TIME');
static func time_inc(value:int = 1)->void:
	_time_set(time_get()+value);
static func _time_set(value:int)->void:
	if value < 1:
		return;
	_g_params.TIME = value;
static func day()->int:
	return int(time_get()/1440.0);
static func is_start_new_day()->bool:
	return time_get()%1440 == 0;

static func dialogue_get()->Control:
	return _g_params.get('DIALOGUE');
static func dialogue_set(value:Control)->void:
	_g_params.DIALOGUE = value;

static func dashboard_get()->Control:
	return _g_params.get('DASHBOARD');
static func dashboard_set(value:Control)->void:
	_g_params.DASHBOARD = value;

static func place_get()->Node2D:
	return _g_params.get('PLACE');
static func place_set(value:Node2D)->void:
	_g_params.PLACE = value;

static func readboard_get()->Control:
	return _g_params.get('READBOARD');
static func readboard_set(value:Control)->void:
	_g_params.READBOARD = value;


static func vset(name:String, variable)->void:
	_g_params[name] = variable;
static func vget(name:String):
	return _g_params.get(name);

static func factorial(i:int)->int:
	for j in range(1, i):
		i*=j;
	return i;

static func println(text:String)->void:
	print('Time ',time_get(),': ',text);

static func print_err(text:String)->void:
	print('ERR: ', text);
