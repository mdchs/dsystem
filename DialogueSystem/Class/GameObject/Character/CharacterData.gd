extends CharacterStatic

## Класс-хранилище информации о персонаже
class_name CharacterData

signal LocationExit;

var _name:String setget _name_set, name_get;
func _name_set(value:String)->void:
	_name = value;
func name_get()->String:
	return _name;

var _messages:Array setget ,messages_get;
func messages_get()->Array:
	return _messages;
func message_add(value:MessageData)->void:
	_messages.append(value);
func message_erase(value:MessageData)->void:
	_messages.erase(value);

var _image:String setget _image_set, image_get;
func _image_set(value:String)->void:
	_image = value;
func image_get()->String:
	return _image;

var _location:LocationData setget location_set, location_get;
func location_set(value:LocationData)->void:
	self._location_character_erase(_location, self);
	self.emit_signal('LocationExit');
	_location = value;
	self._location_character_add(_location, self);
	_location.emit_signal('LocationEnter', self);
	if CharacterStatic.player_get() == self:
		LocationStatic.player_position_set(_location);
func location_get()->LocationData:
	return _location;

var _profs:Professions setget ,profs_get;
func profs_get()->Professions:
	return _profs;

##сделать слоты + массив для сортировки?
var _inventory:Inventory setget ,inventory_get;
func inventory_get()->Inventory:
	return _inventory;

func money_get()->int:
	return self.inventory_get().count_get(ItemStatic.money_item_get());
func money_add(value:int)->void:
	self.inventory_get().add(ItemStatic.money_item_get(),
							value);
func money_dec(value:int)->bool:
	return self.inventory_get().dec(ItemStatic.money_item_get(),
							value);

var _knowledge:Knowledge setget ,knowledge_get;
func knowledge_get()->Knowledge:
	return _knowledge;

static func character_or_anchor_get(cobject)->CharacterStatic:
	if (cobject is CharacterStatic):
		return cobject;
	return anchor_get(str(cobject));

func _init(name:String, 
		image:String,
		messages:Array,
		location:LocationData,
		professions:Dictionary,
		inventory:Dictionary,
		knowledge:PoolStringArray,
		anchor:String='')->void:
	self._name_set(name);
	self._image_set(image);
	for message in messages:
		self.message_add(message);
	self._anchor_add(anchor, self);
	self.location_set(location);
	_profs = Professions.new(professions);
	_inventory = Inventory.new(inventory);
	_knowledge = Knowledge.new(knowledge);
