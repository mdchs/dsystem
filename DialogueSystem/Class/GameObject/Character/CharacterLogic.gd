extends CharacterData

## Класс для сложной работы с CharacterData
class_name CharacterLogic

func _init(name:String, 
		image:String,
		messages:Array,
		location:LocationData,
		professions:Dictionary,
		inventory:Dictionary,
		knowledge:PoolStringArray,
		anchor:String=''
		).(
		name,
		image,
		messages,
		location,
		professions,
		inventory,
		knowledge,
		anchor
	)->void:
	pass;

## скрафтить рецепт с помощью профессии
func profession_work(profession:ProfessionData,
					receipt:Receipt,
					count_work:int)->bool:
	if self.profs_get().count_get(profession) < receipt.need_exp_get():
		return false;
	var result:bool = profession.work(receipt, 
							self.inventory_get(),
							count_work);
	if result:
		self.profs_get().add(profession, count_work*receipt.gain_exp_get())
	return result;

## использовать предмет
func item_use(item:ItemData, count:int = 1)->bool:
	if !self.inventory_get().dec(item, count):
		return false;
	item.actions_exec(self);
	return true;

## продать предмет
func item_sale(
	item:ItemData,
	count:int,
	price:int)->bool:
	return StockExchangeStatic.bid_add(self,
								item,
								count,
								price);

## купить предмет
func item_buy(item:ItemData,
			max_count:int)->bool:
	return StockExchangeStatic.buy(self,
									item,
									max_count);

## оценить предмет
func item_estimate(item:ItemData)->int:
	var price:int=item.price_get();
	return int(max(((item.demand_coef_get()+1.0)*price),price));

## ждать
func wait(_time:int)->bool:
	return true;

##переместится в локацию
func location_move(location:LocationData, _time:int)->void:
	self.location_set(location);
	#if self.wait(time):
	#	pass;
