extends ConfigParser

## парсер конфига персонажей
class_name CharacterConfigParser

const _BASE_CHARACTERS_PATH:String = 'res://Res/Configs/characters.ds';

const TAGS:Array = [
	'[NAME]',
	'[ANCHOR]',
	'[IMAGE]',
	'[MESSAGES]',
	'[LOCATION]',
	'[PRIORITY]',
	'[PROFESSIONS]',
	'[INVENTORY]',
	'[KNOWLEDGE]'
]

## основная функция конфига, читающая персонажей подряд
static func load_config()->bool:
	print('===Characters===');
	var file:FileWorker = FileWorker.new();
	if file.open(_BASE_CHARACTERS_PATH, File.READ) != OK:
		.print_err(file.line_position_get(),'characters.ds file not found');
		return false;
	while file.position_get() < file.lenght():
# warning-ignore:return_value_discarded
		if !_parse(file):
			.print_err(file.line_position_get(),'characters.ds failed load ');
			file.close();
			return false;
	if !CharacterStatic.player_get():
		.print_err(file.line_position_get(),'Character Anchor @p no found.');
		return false;
	.print_err(file.line_position_get(),'characters.ds successfully load');
	file.close();
	return true;

## разбор персонажа по тегам и преобразования текста в структуры данныъ
static func _parse(file:FileWorker)->bool:
	var line_pos:int = file.line_position_get()+1;
	var params:PoolStringArray = ._tag_parse(file, TAGS);
	if !params.size():
		.print_err(line_pos,'Line is empty?');
		return false;
	if !params[0]:
		.print_err(line_pos,'[NAME] %s is empty'%[params[0]]);
		return false;
	line_pos+=1;
	if params[1].length() < 2:
		.print_err(line_pos,'[ANCHOR] %s is empty'%[params[1]]);
		return false;
	elif !params[1].begins_with('@'):
		.print_err(line_pos,'[ANCHOR] %s must start with @'%[params[1]]);
		return false;
	elif ['@u','@i'].has(params[1]):
		.print_err(line_pos,'[ANCHOR] %s cannot be named @u, @i'%[params[1]]);
		return false;
	line_pos+=1;
	if params[2].length()<6:
		.print_err(line_pos,'[IMAGE] %s is empty'%[params[2]]);
		return false;
	elif !params[2].ends_with('.png'):
		.print_err(line_pos,'[IMAGE] are not in PNG format'%[params[2]]);
		return false;
	var path:String = Config.CHARACTER_IMAGE_PATH+params[2];
	if !Config.DEFAULT_DIRECTORY.file_exists(path):
		.print_err(line_pos,'[IMAGE] not found: %s'%[path]);
		return false;
	var messages:Array = [];
	if params[3]:
		line_pos+=1;
		for manchor in params[3].split(',',false):
			var message:MessageData = MessageData.anchor_get(manchor);
			if !message:
				.print_err(line_pos,'[MESSAGES] %s. Message anchor %s not found'%[params[3],manchor]);
				return false;
			messages.append(MessageData.anchor_get(manchor));
	line_pos+=1;
	var location:LocationData = LocationStatic.anchor_get(params[4]);
	if !location:
		.print_err(line_pos,'[LOCATION] %s is empty'%[params[4]]);
		return false;
	line_pos+=1;
	var priorities:PoolIntArray = ._priority_parse(line_pos,params[5]);
	if !priorities.size():
		return false;
	var professions:Dictionary = {};
	if params[6]:
		line_pos+=1;
		for prof in params[6].split(',',false):
			var prof_param:PoolStringArray = prof.split(':');
			var profession:ProfessionData = ProfessionStatic.anchor_get(prof_param[0]);
			if !profession:
				.print_err(line_pos,'[PROFESSIONS] %s.Profession anchor %s not found'%[params[6],prof_param[0]]);
				return false;
			professions[profession] = int(prof_param[1]);
	var inventory:Dictionary = {};
	if params[7]:
		line_pos+=1;
		inventory = _inventory_parse(line_pos,params[7]);
		if !inventory.size():
			return false;
	var knowledge:PoolStringArray = [];
	if params[8]:
		line_pos+=1;
		knowledge = _knowledge_parse(line_pos, params[8]);
		if !knowledge.size():
			.print_err(line_pos,'[KNOWLEDGE] %s is invalid '%[params[8]]);
			return false;
	CharacterCalled.new(params[0],
						params[2],
						messages,
						location,
						priorities,
						professions,
						inventory,
						knowledge,
						params[1]);
	return true;
