extends CharacterLogic

## Класс-Интерфейс для работы функций написанных в конфиге
class_name CharacterCalled

####################################################
###                   БАЗА                       ###
####################################################
## Получить имя персонажа
func name()->String:
	return .name_get();
## Сравнить персонажа
func iam(cobject)->bool:
	var character:CharacterData = self.character_or_anchor_get(cobject);
	if !character:
		.print_err('Function "iam" not found Character Anchor %s'%[cobject]);
		return false;
	return self == character;
## Переместить персонажа в локацию
func move(lobject, time:int = 0)->void:
	var location:LocationData = LocationData.location_or_anchor_get(lobject);
	if !location:
		.print_err('Function "move" not found Loaction Anchor %s'%[lobject]);
		return;
	self.location_move(location, time);
##Добавить знание
func mems(name:String, is_know:bool=true)->void:
	self.knowledge_get().add(name, is_know);
##Получить есть у персонажа знание или нет
func memg(name:String)->bool:
	return self.knowledge_get().get(name);
####################################################
###                СООБЩЕНИЯ                     ###
####################################################
## Удалить сообщение
func msgd(mobject)->void:
	var message:MessageData = MessageData.message_or_anchor_get(mobject);
	if !message:
		.print_err('Function "msgd" not found Message Anchor %s'%[mobject]);
		return;
	.message_erase(message);
## Добавить сообщение
func msga(mobject)->void:
	var message:MessageData = MessageData.message_or_anchor_get(mobject);
	if !message:
		.print_err('Function "msga" not found Message Anchor %s'%[mobject]);
		return;
	.message_add(message);

####################################################
###                  НУЖДЫ                       ###
####################################################
## Взять приоритет персонажа по коду нужды
func nprg(need_code:String)->int:
	if !SoulsContoller.is_active():
		.print_err('Soul Controller not active!');
		return 0;
	var need_id:int = NeedStatic.code_get_id(need_code);
	if !need_id == -1:
		.print_err('Function "nprg" not found NeedCode %s'%[need_code]);
		return 0;
	var soul:Soul = SoulsContoller.character_soul_get(self);
	return soul.priorities_get().get_by_need(need_id);
## Взять максимально приоритетную нужду
func npmg()->String:
	if !SoulsContoller.is_active():
		.print_err('Soul Controller not active!');
		return '';
	var soul:Soul = SoulsContoller.character_soul_get(self);
	return NeedStatic.id_get_code(soul.priority_max_need_id_get());
### Взять эмоцию по коду нужды
func nemg(need_code:String)->int:
	if !SoulsContoller.is_active():
		.print_err('Soul Controller not active!');
		return 0;
	var need_id:int = NeedStatic.code_get_id(need_code);
	if !need_id == -1:
		.print_err('Function "nemg" not found NeedCode %s'%[need_code]);
		return 0;
	var soul:Soul = SoulsContoller.character_soul_get(self);
	return soul.stat_get(need_id).max_emotion_get();


####################################################
###               ПРЕДМЕТЫ/РЫНОК                 ###
####################################################
## выставить заявку на рынок (продать предмет)
func bida(iobject,
		count:int,
		price:int)->bool:
	var item:ItemData = ItemData.item_or_anchor_get(iobject);
	if !item:
		.print_err('Function "bida" not found Item Anchor %s'%[iobject]);
		return false;
	elif count <=0:
		.print_err('Function "bida" count <=0');
		return false;
	elif price <=0:
		.print_err('Function "bida" price <=0');
		return false;
	return self.item_sale(item,count,price);
## закрыть все заявки на рынке
func bidc(iobject)->void:
	var item:ItemData = ItemData.item_or_anchor_get(iobject);
	if !item:
		.print_err('Function "bidc" not found Item Anchor %s'%[iobject]);
		return;
	StockExchangeStatic.bid_close_all_by_character(
				self,
				item);
## купить предмет на рынке
func bidb(iobject,
		max_count:int)->bool:
	var item:ItemData = ItemData.item_or_anchor_get(iobject);
	if !item:
		.print_err('Function "bidb" not found Item Anchor %s'%[iobject]);
		return false;
	elif max_count <= 0:
		.print_err('Function "bidb" price <=0');
	return self.item_buy(item,
					max_count);
## оценить стоимость предмета
func iest(iobject)->int:
	var item:ItemData = ItemData.item_or_anchor_get(iobject);
	if !item:
		.print_err('Function "iest" not found Item Anchor %s'%[iobject]);
		return 0;
	return self.item_estimate(item);
## использовать предмет
func use(iobject)->bool:
	var item:ItemData = ItemData.item_or_anchor_get(iobject);
	if !item:
		.print_err('Function "use" not found Item Anchor %s'%[iobject]);
		return false;
	return self.item_use(item);

####################################################
###                  РАБОТА                      ###
####################################################
## Выполнить работу (завершить)
func work(pobject,
		robject,
		count:int)->bool:
	var receipt:Receipt = Receipt.receipt_or_anchor_get(robject);
	var prof:ProfessionData = ProfessionData.profession_or_anchor_get(pobject);
	if !receipt:
		.print_err('Function "work" not found Receipt Anchor %s'%[robject]);
		return false;
	elif !prof:
		.print_err('Function "work" not found Profession Anchor %s'%[pobject]);
		return false;
	return self.profession_work(
				prof,
				receipt,
				count);

func _init(name:String, 
			image:String,
			messages:Array,
			location:LocationData,
			priority:PoolIntArray,
			professions:Dictionary,
			inventory:Dictionary,
			knowledge:PoolStringArray,
			anchor:String=''
			).(
			name,
			image,
			messages,
			location,
			professions,
			inventory,
			knowledge,
			anchor
	)->void:
	SoulsContoller._character_soul_add(self, priority);
	if CharacterStatic.player_get() == self:
		return;
	TaskTrackerController._tracker_add(self);
