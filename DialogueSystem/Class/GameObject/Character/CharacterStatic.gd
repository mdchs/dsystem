extends GameObject

## Класс для хранения статических конструкций
class_name CharacterStatic

## хранение якорей персонажей
const _anchors_characters:Dictionary = {};
static func _anchor_add(anchor:String, value:CharacterStatic)->void:
	_anchors_characters[hash(anchor)] = value;
static func anchor_get(anchor:String)->CharacterStatic:
	return _anchors_characters.get(hash(anchor));
static func player_get()->CharacterStatic:
	return anchor_get(Config.PLAYER_ANCHOR);
static func interlocutor_get()->CharacterStatic:
	return anchor_get(Config.INTERLOCUTOR_ANCHOR);

## Хранение всех персонажей относительно локации
const _location_characters:Dictionary = {};
static func location_characters_get(location:LocationStatic)->Array:
	return _location_characters.get(location,[]);
static func _location_character_add(location:LocationStatic,
									character:CharacterStatic)->void:
	_location_characters[location] = [character]+location_characters_get(location);
static func _location_character_erase(location:LocationStatic,
									character:CharacterStatic)->void:
	location_characters_get(location).erase(character);
