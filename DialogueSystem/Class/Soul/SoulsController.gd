extends Reference

## Класс для функций по работе с "Душой" Персонажа
class_name SoulsContoller

static func is_active()->bool:
	return Config.NEED_CONTROLLER_GET() == Config.CONTROLLERS.SOUL;

## Словарь для получения "Души" по персонажу
const _character_souls:Dictionary = {};
static func _character_soul_add(character:CharacterData,
								priorities:PoolIntArray)->void:
	if !is_active():
		return;
	_character_souls[character] = Soul.new(priorities);
static func character_soul_get(character:CharacterData)->Soul:
	return _character_souls.get(character);

## каждый день высчитывает новые нужды и удаляет старые, незакрытые
static func autojob()->void:
	if !is_active():
		return;
	if !GlobalObject.is_start_new_day():
		return;
	for character in _character_souls.keys():
		var soul:Soul = _character_souls[character];
		var needs:Array = soul.needs_get();
		needs.invert();
		for need in needs:
			need_failed(character, need);
		for _i in range(5):
			var need:Need = soul.random_need_create();
			if !need:
				continue;
			soul.need_add(need);
	GlobalObject.println('create needs');

## получить последнюю нужду
static func need_get(character:CharacterData)->Need:
	var soul:Soul = character_soul_get(character);
	return soul.last_need_get();

## нужда успешно закрыта
static func need_success(character:CharacterData,
						need:Need)->void:
	var soul:Soul = character_soul_get(character);
	soul.stat_add(need.need_id_get(), 
				need.quality_get(),
				EmotionStatic.TYPE.PLEASURE);  ## надо поработать над эмоциями
	if !soul.need_has(need):
		return;
	soul.need_del(need);

## случились проблемы по пути к выполнению нужды
static func need_problem(character:CharacterData,
						need:Need)->void:
	var soul:Soul = character_soul_get(character);
	if !soul.need_has(need):
		return;
	soul.stat_add(need.need_id_get(), 
				need.quality_get(),
				EmotionStatic.TYPE.ANIXETY); ## надо поработать над эмоциями

## выполнение нужды провалено
static func need_failed(character:CharacterData,
						need:Need)->void:
	var soul:Soul = character_soul_get(character);
	if !soul.need_has(need):
		return;
	soul.stat_add(need.need_id_get(), 
				need.quality_get(),
				EmotionStatic.TYPE.SORROW);  ## надо поработать над эмоциями
	soul.need_del(need);
