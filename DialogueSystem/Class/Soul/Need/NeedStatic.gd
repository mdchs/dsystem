extends Reference

## Статический класс для констант
class_name NeedStatic

## Основные нужды персонажа
const TYPE:Dictionary = {
	AKISITIVE = 0, #акизитивные (накопление ресурсов)
	AESTHETIC = 1, # эстетические (приобрести красивое, стать красивым)
	ALTRUISTIC = 2, #альтруизм (желание помочь)
	HEDONISM = 3, #гедонизм (комфорт)
	GLORYSIM = 4, #глоризм (желание славы)
	GNOSTICISM = 5, #гностические (желание знаний)
	COMMUNICATION = 6, #коммуникативные (поддержка связей и их укрепление)
	PRAXICAL = 7, #праксические (увеличение результативности)
	SCARECROW = 8, #пугнические (быть первым в большем количестве вещей/соревнование)
	ROMANTIC = 9,  #романтические (желание необычного)
}

const TYPE_CODE:Dictionary = {
	'AKI':TYPE.AKISITIVE,
	'AES':TYPE.AESTHETIC,
	'ALT':TYPE.ALTRUISTIC,
	'HED':TYPE.HEDONISM,
	'GLO':TYPE.GLORYSIM,
	'GNO':TYPE.GNOSTICISM,
	'COM':TYPE.COMMUNICATION,
	'PRA':TYPE.PRAXICAL,
	'SCA':TYPE.SCARECROW,
	'ROM':TYPE.ROMANTIC,
	TYPE.AKISITIVE:'AKI',
	TYPE.AESTHETIC:'AES',
	TYPE.ALTRUISTIC:'ALT',
	TYPE.HEDONISM:'HED',
	TYPE.GLORYSIM:'GLO',
	TYPE.GNOSTICISM:'GNO',
	TYPE.COMMUNICATION:'COM',
	TYPE.PRAXICAL:'PRA',
	TYPE.SCARECROW:'SCA',
	TYPE.ROMANTIC:'ROM',
}

static func code_get_id(code:String)->int:
	return TYPE_CODE.get(code,-1);
static func id_get_code(id:int)->String:
	return TYPE_CODE.get(id,'');


## Словарь выдающий количество нужд по качеству и идентификатору нужды
const _quality_count:Dictionary = {};
static func _quality_count_inc(need_id:int,
							quality:int,
							inc:int = 1)->void:
	_quality_count[_path(need_id,quality)] = _quality_count_get(need_id, quality)+inc;
static func _quality_count_dec(task, dec:int = 1)->void:
	_quality_count_inc(task, -dec);
static func _quality_count_get(need_id:int,
							quality:int)->int:
	return _quality_count.get(_path(need_id,quality),0);

static func _path(need_id:int, quality:int)->int:
	return hash('%s_%s'%[need_id,quality]);
