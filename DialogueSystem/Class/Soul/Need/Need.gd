extends NeedStatic

class_name Need

var _lifetime:int setget _lifetime_set, lifetime_get;
func _lifetime_set(value:int)->void:
	_lifetime = value+GlobalObject.time_get();
func lifetime_get()->int:
	return _lifetime;
func time_left()->int:
	return self.lifetime_get()-GlobalObject.time_get();

var _need_id:int setget _need_id_set, need_id_get;
func _need_id_set(value:int)->void:
	_need_id = value;
func need_id_get()->int:
	return _need_id;

var _quality:int setget _quality_set, quality_get;
func _quality_set(value:int)->void:
	_quality = value;
func quality_get()->int:
	return _quality;

## сохранение текущей эмоции по нужде, чтобы в зависимости от неё
## была возможность испытывать другие эмоции
var _emotion_id:int = EmotionStatic.TYPE.CONFIDENCE setget _emotion_id_set, emotion_id_get;
func _emotion_id_set(value:int)->void:
	_emotion_id = value;
func emotion_id_get()->int:
	return _emotion_id;

## объект поиска
var _object:GameObject setget _object_set, object_get;
func _object_set(value:GameObject)->void:
	_object = value;
func object_get()->GameObject:
	return _object;

## количество объектов
var _count:int setget _count_set, count_get;
func _count_set(value:int)->void:
	_count = value;
func count_get()->int:
	return _count;

func _init(
		need_id:int=0,
		quality:int=0,
		count:int=0,
		lifetime:int=0)->void:
	self.reload(need_id,
				quality,
				count,
				lifetime);

func reload(
		need_id:int,
		quality:int,
		count:int,
		lifetime:int)->void:
	self._need_id_set(need_id);
	self._quality_set(quality);
	self._count_set(count);
	self._lifetime_set(lifetime);

## подобрать объект под решение этой нужды
func object_find()->bool:
	var items:Array = ItemStatic.items_get_by_quality(
									self.need_id_get(), 
									self.quality_get());
	if !items.size():
		return false;
	var item:ItemData = items.back();
	self._object_set(item);
	return true;
