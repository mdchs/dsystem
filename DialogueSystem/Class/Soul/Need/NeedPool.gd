extends Reference

## Пул для нужд, чтобы не фрагментировать память во время 
## работы приложения
class_name NeedsPool

const _needs_pool:Array = [];
static func needs_pool_load(start_size:int)->void:
	for _i in range(0,start_size):
		_need_pool_add(Need.new());
static func _need_pool_add(need:Need)->void:
	_needs_pool.append(need);
static func _need_pool_get(need_id:int,
						quality:int,
						count:int,
						lifetime:int)->Need:
	var need:Need = _needs_pool.pop_back();
	if !need:
		return Need.new(need_id, 
						quality, 
						count,
						lifetime);
	need.reload(need_id, 
				quality, 
				count,
				lifetime);
	return need;
