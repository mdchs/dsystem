extends Reference

## Класс "Души" персонажа генерирующий нужды и собирающий статистику
## о его эмоциях (резульата выполнения нужд)
class_name Soul

var _priorities:Priorities setget _priorities_set, priorities_get;
func _priorities_set(value:Priorities)->void:
	_priorities = value;
func priorities_get()->Priorities:
	return _priorities;
func priority_max_need_id_get()->int:
	return self.priorities_get().max_need_id_get();

var _stats:Array = [];
func stat_get(need_id:int)->EmotionStat:
	return _stats[need_id];
func stat_add(need_id:int, 
			quality:int, 
			emotion_id:int)->void:
	var stat:EmotionStat = self.stat_get(need_id);
	stat.stat_inc(emotion_id, quality);

var _needs:Array = [];
func need_add(value:Need)->void:
	_needs.push_front(value);
	NeedStatic._quality_count_inc(value.need_id_get(),
								value.quality_get());
func needs_get()->Array:
	return _needs;
func need_has(value:Need)->bool:
	return _needs.has(value);
func need_del(value:Need)->void:
	_needs.erase(value);
	NeedsPool._need_pool_add(value);
	NeedStatic._quality_count_dec(value.need_id_get(),
								value.quality_get());
func last_need_get()->Need:
	if !_needs.size():
		return null;
	return _needs.back();

func random_need_create()->Need:
	var rand:int = randi()%self.priorities_get().sum_get();
	var total:int = 0;
	for priority in self.priorities_get().size():
		total+=self.priorities_get().get_by_need(priority);
		if total < rand:
			continue;
		return NeedsPool._need_pool_get(
						priority, 
						self.stat_get(priority).quality_get(),
						randi()%3+1,
						1440);
	return null;
	
func _init(priorities:PoolIntArray)->void:
	self._priorities_set(Priorities.new(priorities));
	for _i in self.priorities_get().size():
		_stats.append(EmotionStat.new());
