extends EmotionStatic

## Сатистика эмоций 
class_name EmotionStat

## количество добавленых эмоций
var _count:int = 0;
## идентификатор максимальной эмоции
var _max_emotion_id:int = 0;
## сумма качественности предметов вызывающих эмоцию
var _quality_sum:int;
## массив эмоций
var _emotions:PoolIntArray = [0,0,0,0,0,0,0,0] setget ,_emotions_get;
func _emotions_get()->PoolIntArray:
	return _emotions;
func _emotion_get(emotion_id:int)->int:
	return _emotions[emotion_id];
func stat_inc(emotion_id:int,
			quality:int)->void:
	_emotions[emotion_id] +=1;
	if _emotions[emotion_id] > _emotions[_max_emotion_id]:
		_max_emotion_id = emotion_id;
	_count +=1;
	_quality_sum += quality;
func max_emotion_get()->int:
	return _max_emotion_id;
func get_percent(emotion_id:int)->float:
	if !_count:
		return 0.0;
	return 1.0*self._emotion_get(emotion_id)/_count;
func quality_get()->int:
	if !_count:
		return 1;
	return int(round(1.0*_quality_sum/_count));
