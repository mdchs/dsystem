extends Reference

## Статический класс для констант 
class_name EmotionStatic

const TYPE:Dictionary = {
	CONFIDENCE = 0,   # Уверенность
	ANIXETY = 1,      # Беспокойство
	ANGER = 2,        # Гнев
	SORROW = 3,       # Печаль
	PLEASURE = 4,     # удовлетворение
	DISPLEASURE = 5,  # Неудовлетворение
	DESIRE = 6,       # Желание
	DISGUST = 7       # Овращение
}


const TYPE_CODE:Dictionary = {
	'CON':TYPE.CONFIDENCE,
	'ANI':TYPE.ANIXETY,
	'ANG':TYPE.ANGER,
	'SOR':TYPE.SORROW,
	'PLE':TYPE.PLEASURE,
	'DPL':TYPE.DISPLEASURE,
	'DSI':TYPE.DESIRE,
	'DSG':TYPE.DISGUST,
}

static func code_get_id(code:String)->int:
	return TYPE_CODE.get(code,-1);
