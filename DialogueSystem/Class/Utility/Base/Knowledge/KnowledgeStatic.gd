extends Reference

class_name KnowledgeStatic

const _knowledge_names:Dictionary = {};
static func _knowledge_name_indx_get(name:String)->int:
	var namehash:int = hash(name);
	var size:int = _knowledge_names.get(namehash,-1);
	if size < 0:
		size = _knowledge_names.size();
		_knowledge_names[namehash] = size;
	return size;
