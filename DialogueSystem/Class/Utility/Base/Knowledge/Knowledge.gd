extends KnowledgeStatic

class_name Knowledge

var _bitmap:BitMap = BitMap.new() setget ,_bitmap_get;
func _bitmap_get()->BitMap:
	return _bitmap;
func add_array(names:PoolStringArray, is_know:bool=true)->void:
	for elem in names:
		self.add(elem, is_know);
func add(name:String, is_know:bool=true)->void:
	var indx:int = self._knowledge_name_indx_get(name);
	if self._bitmap_get().get_size().y <= indx:
		var new_bitmap:BitMap = BitMap.new();
# warning-ignore:integer_division
		new_bitmap.create(Vector2(1,64*int(indx/64)+64));
		for pos in range(self._bitmap_get().get_size().y):
			if !self._bitmap_get().get_bit(Vector2(0,pos)):
				continue;
			new_bitmap.set_bit(Vector2(0,pos), true);
		_bitmap = new_bitmap;
	self._bitmap_get().set_bit(Vector2(0,indx),is_know);
func get(name:String)->bool:
	var indx:int = self._knowledge_name_indx_get(name);
	if self._bitmap_get().get_size().y <= indx:
		return false;
	return self._bitmap_get().get_bit(Vector2(0,indx));

func _init(names:PoolStringArray)->void:
# warning-ignore:integer_division
	self._bitmap_get().create(Vector2(1,64*int(names.size()/64)+64));
	self.add_array(names);
