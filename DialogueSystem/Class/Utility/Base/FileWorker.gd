extends Reference

## Класс для работы с файлами
class_name FileWorker

enum {
	READ = 1 ,
	WRITE = 2,
	READ_WRITE = 3,
	WRITE_READ = 7

}

var _file:File setget _file_set, _file_get;
func _file_set(value:File)->void:
	_file = value;
func _file_get()->File:
	return _file;
func lenght()->int:
	return self._file_get().get_len();

var _line_position:int=0 setget _line_position_set, line_position_get;
func _line_position_set(value:int)->void:
	_line_position = value;
func _line_position_inc(value:int = 1)->void:
	_line_position += value;
func line_position_get()->int:
	return _line_position;

var _last_position:int=0 setget _last_position_set, last_position_get;
func _last_position_set(value:int)->void:
	_last_position = value;
func last_position_get()->int:
	return _last_position;
func position_get()->int:
	return self._file_get().get_position();

func open(path:String, mode_flag:int)->int:
	return self._file_get().open(path, mode_flag);

func close()->void:
	self._file_get().close();

func line_get()->String:
	self._last_position_set(self.position_get());
	self._line_position_inc();
	return self._file_get().get_line();

func move_start_line()->void:
	self._line_position_inc(-1);
	self._file_get().seek(self.last_position_get());

func _init()->void:
	self._file_set(File.new());
