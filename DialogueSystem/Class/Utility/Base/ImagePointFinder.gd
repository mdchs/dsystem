extends Reference

class_name ImagePointFinder

static func PointsFind(image:Image)->Dictionary:
	image.lock();
	var dict:Dictionary = {};
	for x in image.get_width():
		var y:int=0;
		while y < image.get_height():
			var color:Color = image.get_pixel(x,y);
			if color.a == 0:
				y+=15;
				continue;
			while image.get_pixel(x,y).a != 0:
				y-=1;
			var pos:Vector2 = Vector2(x,y);
			var next_y:int = _point_has(pos, dict);
			if next_y:
				y += next_y;
				continue;
			var rect:Rect2 = _find_element(image, pos, color);
			if rect.size < Vector2.ONE:
				print('Min Rect!')
				image.unlock();
				return {};
			dict[rect] = color;
			y +=rect.size.y;
	image.unlock();
	return dict;

static func _point_has(point:Vector2, dict:Dictionary)->int:
	for rect in dict.keys():
		if (rect as Rect2).has_point(point):
			return rect.size.y;
	return 0;

static func _find_element(
						image:Image,
						pos:Vector2, 
						color:Color 
						)->Rect2:
	var rect:Rect2 = Rect2(pos, Vector2.ZERO);
	var x:int = int(pos.x);
	var y:int = int(pos.y+1);
	var find_color:Color = image.get_pixel(x,y);
	while find_color == color and y < image.get_height():
		y+=5;
		find_color = image.get_pixel(x,y);
	while find_color != color:
		y-=1
		find_color = image.get_pixel(x,y);
	while find_color == color and x < image.get_width():
		x+=5;
		find_color = image.get_pixel(x,y);
	while find_color != color:
		x-=1
		find_color = image.get_pixel(x,y);
	rect.size = Vector2(x+1,y+1)-pos;
	return rect;
