extends Reference

## Базовый класс-хранилище на основании Словаря
## Основной принцип работы заключается в том, что на каждый объект
## не требуется создавать отдельный класс. 
##Часто нам важно просто количество объектов какого-то класса
class_name StorageStack

var _storage:Dictionary setget _set_all,get_all;
func _set_all(value:Dictionary)->void:
	_storage = value.duplicate();
func _set_item(value, count:int)->void:
	_storage[value] = count
func _add(value, count:int = 1)->void:
	self._set_item(value, self._count_get(value)+count);
func _dec(value, count:int = 1)->bool:
	var diff:int = self._count_get(value) - count;
	if diff < 0:
		return false
	elif !diff:
		return _storage.erase(value);
	self._set_item(value, diff);
	return true;
func _count_get(value)->int:
	return _storage.get(value,0);
func get_all()->Dictionary:
	return _storage;
func size()->int:
	return _storage.size();

func _init(dict:Dictionary = {})->void:
	self._set_all(dict);
