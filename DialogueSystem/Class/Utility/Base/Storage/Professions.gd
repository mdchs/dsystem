extends StorageStack

## Класс для профессий персонажа. Содержит ссылку на профессию и количество опыта в ней
class_name Professions

func _set_all(value:Dictionary)->void:
	._set_all(value);
func add(value:ProfessionData, count:int = 1)->void:
	._add(value, count);
func dec(value:ProfessionData, count:int = 1)->bool:
	return ._dec(value, count);
func count_get(value:ProfessionData)->int:
	return ._count_get(value);

func _init(dict:Dictionary = {})->void:
	self._set_all(dict);
