extends StorageStack

## Класс для инвентаря персонажей. Содержит ссылку на предмет и его количество
class_name Inventory

func _set_all(value:Dictionary)->void:
	._set_all(value);
	self._max_count_calc();
func add(value:ItemData, count:int = 1)->void:
	._add(value, count);
	if count > self.count_get(_item_max_count):
		_item_max_count = value;
func dec(value:ItemData, count:int = 1)->bool:
	var is_completed = ._dec(value, count);
	if is_completed and value == _item_max_count:
		self._max_count_calc();
	return is_completed;
func count_get(value:ItemData)->int:
	return ._count_get(value);
var _item_max_count:ItemData;
func item_by_max_count_get()->ItemData:
	return _item_max_count;
func _max_count_calc()->void:
	for item in self.get_all().keys():
		if self.count_get(_item_max_count) < self.count_get(item):
			_item_max_count = item;
func items_get()->Array:
	return self.get_all().keys();

func _init(dict:Dictionary = {})->void:
	self._set_all(dict);
