extends Enumerations

## Класс для качеств GameObject
class_name Qualities

func _set_all(value:PoolIntArray)->void:
	if value.size() != 10:
		push_error('Размер массива != 10')
		return;
	._set_all(value);
	for need_id in self.size():
		if self.get_by_need(need_id) > self.get_by_need(self.need_max_get_get()):
			_need_id_with_max_quality = need_id;
func get_by_need(need_id:int)->int:
	return ._get_by_id(need_id);

var _need_id_with_max_quality:int=0;
func need_max_get_get()->int:
	return _need_id_with_max_quality;
func max_get()->int:
	return self.get_by_need(self.need_max_get_get());

func _init(value:PoolIntArray).(value)->void:
	pass;
