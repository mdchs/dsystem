extends Enumerations

## Класс приоритетов персонажа.
class_name Priorities

var _sum:int = 0;
var _max_need_id:int = 0 setget ,max_need_id_get;
func max_need_id_get()->int:
	return _max_need_id;
func sum_get()->int:
	return _sum;
func _set_all(value:PoolIntArray)->void:
	if value.size() != 10:
		push_error('Размер массива != 10')
		return;
	._set_all(value);
	_sum=self.get_by_need(0);
	for i in range(1,self.size()):
		_sum+=self.get_by_need(i);
		if self._get_by_id(self.max_need_id_get()) < self._get_by_id(i):
			_max_need_id = i;
func get_by_need(need_id:int)->int:
	return ._get_by_id(need_id);

func _init(value:PoolIntArray).(value)->void:
	pass;
