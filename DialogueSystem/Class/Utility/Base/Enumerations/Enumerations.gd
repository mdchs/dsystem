extends Reference

## Базовый класс перечисления. Обертка вокруг массива int[]
##с функциями на основные случаи жизни 
class_name Enumerations

var _enums:PoolIntArray setget _set_all, get_all;
func _set_all(value:PoolIntArray)->void:
	_enums = value;
func get_all()->PoolIntArray:
	return _enums;
func _get_by_id(id:int)->int:
	return self.get_all()[id];
func size()->int:
	return _enums.size();

func _init(value:PoolIntArray)->void:
	self._set_all(value);
