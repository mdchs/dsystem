extends Reference

## Базовый класс для парсинга конфигов
class_name ConfigParser

## парс тегов
static func _tag_parse(file:FileWorker,
					tags:PoolStringArray)->PoolStringArray:
	var params:PoolStringArray = [];
	var line:String = file.line_get();
	while line.begins_with('#') or !line.length():
		while line.begins_with('#') or !line.length():
			if file.position_get() < file.lenght():
				line = file.line_get();
			else:
				return PoolStringArray();
	if !line.length():
		return params;
	params.resize(tags.size());
	var skip_last_row:bool=false;
	for tag_id in tags.size():
		if !line.begins_with(tags[tag_id]):
			if tag_id == tags.size()-1:
				skip_last_row = true;
			continue;
		var pos:int = line.find('=')+1;
		if !pos:
			print_err(file.line_position_get(),'Invalid tag pattern in line %s'%[line])
			return PoolStringArray();
		params[tag_id] = _clear_char(line.substr(pos),' ');
		if tag_id < tags.size()-1:
			line = file.line_get().trim_prefix('	');
	if skip_last_row:
		file.move_start_line();
	return params;

## парс знаний
static func _knowledge_parse(_line:int, text:String)->PoolStringArray:
	return text.split(',',false);

## парс приоритетов
static func _priority_parse(line:int, text:String)->PoolIntArray:
	var sum:int= 0;
	var priorities:PoolIntArray = [0,0,0,0,0,0,0,0,0,0];
	var priorities_arr:PoolStringArray = text.split(',',false);
	for id in priorities_arr.size():
		var p = _base_variable_convert(priorities_arr[id]);
		if typeof(p) != TYPE_INT:
			print_err(line,'PRIORITY %s invalid type int'%[p]);
			return PoolIntArray();
		if p < 0 or p > 100:
			print_err(line,'Priority should be between 0 < %s < 100'%[p]);
			return PoolIntArray();
		priorities[id] = p;
		sum+=priorities[id];
	if !sum:
		print_err(line,'Priorities is empty or not set %s'%[text]);
		return PoolIntArray();
	return priorities;

## парс качеств
static func _quality_parse(line_pos:int, text:String)->PoolIntArray:
	if !text.length():
		print_err(line_pos,'[QUALITIES] %s is empty'%[text]);
		return PoolIntArray();
	var qualities:PoolIntArray = [0,0,0,0,0,0,0,0,0,0];
	var qualities_arr:PoolStringArray = text.split(',',false);
	if !qualities_arr.size():
		print_err(line_pos,'[QUALITIES] %s is empty'%[text]);
		return PoolIntArray();
	for id in qualities_arr.size():
		var q = _base_variable_convert(qualities_arr[id]);
		if typeof(q) != TYPE_INT:
			print_err(line_pos,'[QUALITIES] %s. QUALITY %s invalid type int'%[text,q]);
			return PoolIntArray();
		if q < 0 or q > 5:
			print_err(line_pos,'[QUALITIES] %s. QUALITY should be between 0 < %s < 5'%[text,q]);
			return PoolIntArray();
		qualities[id] = q;
	return qualities;

## парс перечисления предметов с их количеством
static func _inventory_parse(line_pos:int, text:String)->Dictionary:
	var dict:Dictionary = {};
	for elem in text.split(',',false):
		var item_param:PoolStringArray = elem.split(':');
		if item_param.size() != 2:
			print_err(line_pos,'Expected ITEM:ITEM_COUNT %s'%[item_param]);
			return {};
		var i:ItemData = ItemStatic.anchor_get(item_param[0]);
		if !i:
			print_err(line_pos,'ITEM anchor %s not found'%[item_param[0]]);
			return {};
		var count = _base_variable_convert(item_param[1]);
		if typeof(count) != TYPE_INT:
			print_err(line_pos,'ITEM_COUNT %s invalid type int'%[item_param]);
			return {};
		dict[i] = count;
	return dict;

## парс координат
static func _coord_parse(line_pos:int, text:String)->Vector2:
	var coord_string:Array = text.split(':',false);
	if coord_string.size() != 2:
		print_err(line_pos,'COORD %s is not correct'%[coord_string]);
		return Vector2.INF;
	var x = _base_variable_convert(coord_string[0]);
	if typeof(x) != TYPE_INT:
		print_err(line_pos,'COORD X %s invalid type int'%[x]);
		return Vector2.INF;
	var y = _base_variable_convert(coord_string[1]);
	if typeof(y) != TYPE_INT:
		print_err(line_pos,'COORD Y %s invalid type int'%[x]);
		return Vector2.INF;
	return Vector2(x,y);

## парс порталов
static func _portals_parse(line_pos:int,
							text:String, 
							anchor:String)->Array:
	var return_array:Array = [];
	for elem in text.split(',',false):
		var portal_param:PoolStringArray = elem.split(':');
		if portal_param.size() != 2 or portal_param[0].length() < 2:
			print_err(line_pos,'Expected location:weight');
			return [];
		elif portal_param[0].length() < 2:
			print_err(line_pos,'The Location Anchor %s must start with #'%[portal_param[0]]);
			return [];
		elif portal_param[0] == anchor:
			print_err(line_pos,'The location has a transition to itself');
			return [];
		var weight = _base_variable_convert(portal_param[1]);
		if typeof(weight) != TYPE_INT:
			print_err(line_pos,'WEIGHT %s invalid type int'%[weight]);
			return [];
		if weight < 1:
			print_err(line_pos,'WEIGHT should be between %s < 1'%[weight]);
			return [];
		return_array.append([anchor,portal_param[0],weight]);
	return return_array;

## парс точек
static func _points_parse(
						line_pos:int,
						text:String)->Array:
	var points:Array = [];
	for elem in text.split(',',false):
		var points_param:PoolStringArray = elem.split('~');
		if points_param.size() != 2:
			print_err(line_pos,'Expected coord~scale %s'%[elem]);
			return [];
		var coord:Vector2 = _coord_parse(line_pos, points_param[0]);
		if coord == Vector2.INF:
			return [];
		var scale:float = float(points_param[1]);
		if !scale:
			print_err(line_pos,'SCALE not found %s'%[elem]);
			return [];
		points.append([coord, scale]);
	return points;

static func _stuffs_parse(line_pos:int, text:String)->Array:
	var return_array:Array = [];
	for sanchor in text.split(',',false):
		if sanchor:
			var stuff:StuffData = StuffStatic.anchor_get(sanchor);
			if !stuff:
				print_err(line_pos,'STUFF ANCHOR %s not found'%[sanchor]);
				return [];
			return_array.append(stuff);
		else:
			return_array.append(null);
	return return_array;

static func _points_concat_stuffs(
							_line_pos:int,
							points:Array,
							stuffs:Array)->Array:
	var return_array:Array = [];
	for i in points.size():
		var stuff:StuffData;
		if stuffs.size() > i:
			stuff = stuffs[i];
		var point:Point = Point.new(
							points[i][0],
							points[i][1],
							stuff);
		return_array.append(point);
	return return_array;

static func _clear_char(line:String, symbol:String)->String:
	var pos_start:int = 0;
	if !line.length():
		return '';
	while line[pos_start] == symbol and line.length() > pos_start+1:
		pos_start+=1;
	var pos_end:int = line.length()-1;
	while line[pos_end] == symbol and pos_end > pos_start:
		pos_end-=1;
	return line.substr(pos_start,pos_end+1-pos_start);

static func _find_last_seq_pos(line:String, symbol:String)->int:
	var pos:int = -1;
	while line.length() > pos+1 and line[pos+1] == symbol:
		pos+=1;
	return pos;

static func _base_variable_convert(variable:String):
	variable = _clear_char(variable, ' ');
	if ['false','False','F','f'].has(variable):
		return false;
	elif ['true','True','T','t'].has(variable):
		return true;
	elif variable == '0':
		return 0;
	elif float(variable) != 0:
		return int(variable);
	return variable;

static func print_err(line:int, text:String)->void:
	print('Line ',line,': ',text);
