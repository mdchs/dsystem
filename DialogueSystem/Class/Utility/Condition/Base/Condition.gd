extends Reference

## Класс сравнения операндов. Нужен для работы условий в конфиге
## унарный оператор это оператор с одним операндом. Например !variable
class_name Condition

var _first_operand;
func _first_operand_set(value)->void:
	_first_operand = value;
func _first_operand_get():
	return _first_operand;

var _operator:int setget _operatior_set, operator_get;
func _operatior_set(value:int)->void:
	_operator = value;
func operator_get()->int:
	return _operator;

func _init(operator:int, operand)->void:
	self._first_operand_set(operand);
	self._operatior_set(operator);

func exec():
	return null;
