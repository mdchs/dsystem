extends Condition

## Класс сравнения операндов. Нужен для работы условий в конфиге
## унарный оператор это оператор с одним операндом. Например !variable
class_name ConditionUnarVar

func _first_operand_get():
	if (_first_operand is FunctionData) or (_first_operand is Condition):
		return _first_operand.exec();
	return _first_operand;

func _init(
		operator:int, 
		operand
		).(
		operator,
		operand
		)->void:
	pass;

func exec():
	return null;
