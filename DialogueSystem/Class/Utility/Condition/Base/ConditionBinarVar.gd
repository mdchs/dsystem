extends ConditionUnarVar

## Класс сравнения операндов. Нужен для работы условий в конфиге
## унарный оператор это оператор с одним операндом. Например !variable
class_name ConditionBinarVar

var _second_operand;
func _second_operand_set(value)->void:
	_second_operand = value;
func _second_operand_get():
	if (_second_operand is FunctionData) or (_second_operand is Condition):
		return _second_operand.exec();
	return _second_operand;

func _init(
		operator:int, 
		operand,
		operand2
		).(
		operator,
		operand
		)->void:
	self._second_operand_set(operand2);

func exec():
	match self.operator_get():
		TokenStatic.PLUS: 
			return self._first_operand_get()+self._second_operand_get();
		TokenStatic.MINUS:
			return self._first_operand_get()-self._second_operand_get();
		TokenStatic.MULTIPLE:
			return self._first_operand_get()*self._second_operand_get();
	return null;
