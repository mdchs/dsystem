extends ConfigParser

## Класс для парсинга конфига условий
class_name ConditionConfigParser
#
#static func _parse_condition(line_pos:int, line:String)->Condition:
#	line = ._clear_char(line,' ');
#	if line.begins_with('!'):
#		return ConditionNot.new(FunctionConfigParser._parse_operand(line_pos,line.substr(1)));
#	for i in line.length():
#		match line[i]:
#			'>':
#				var operand1 = FunctionConfigParser._parse_operand(line_pos,line.substr(0,i)); 
#				if !operand1:
#					return null;
#				var operand2 = FunctionConfigParser._parse_operand(line_pos,line.substr(i+1));
#				if !operand2:
#					return null;
#				return ConditionMore.new(operand1,operand2);
#			'<': 
#				var operand1 = FunctionConfigParser._parse_operand(line_pos,line.substr(0,i));
#				if !operand1:
#					return null;
#				var operand2 = FunctionConfigParser._parse_operand(line_pos,line.substr(i+1));
#				if !operand2:
#					return null;
#				return ConditionLess.new(operand1,operand2);
#			'=': 
#				if line[i+1] == '=':
#					var operand1 = FunctionConfigParser._parse_operand(line_pos,line.substr(0,i));
#					if !operand1:
#						return null;
#					var operand2 = FunctionConfigParser._parse_operand(line_pos,line.substr(i+2));
#					if !operand2:
#						return null;
#					return ConditionEquals.new(operand1,operand2);
#			'!': 
#				if line[i+1] == '=':
#					var operand1 = FunctionConfigParser._parse_operand(line_pos,line.substr(0,i));
#					if !operand1:
#						return null;
#					var operand2 = FunctionConfigParser._parse_operand(line_pos,line.substr(i+2));
#					if !operand2:
#						return null;
#					return ConditionNotEquals.new(operand1,operand2);
#	.print_err(line_pos, 'Condition not parse: %s'%[line]);
#	return null;

static func parse_condition(line:int,text:String)->ConditionUnarBit:
	var tokens:Array = LexerStatic.analyze(text, line);
	if !tokens.size():
		return null;
	tokens.invert();
	var condition = _parse_formula_or_formula_parenthess(line, tokens);
	if !(condition is ConditionUnarBit):
		.print_err(line,'CONDITION pattern result BIT %s invalid');
		return null;
	return (condition as ConditionUnarBit);

static func _parse_formula_or_formula_parenthess(line:int, tokens:Array):
	var operand = _parse_formula_parenthess(line, tokens);
	if !operand:
		return _parse_formula(line, tokens);
	return operand;

static func _parse_formula_parenthess(line:int, tokens:Array):
	if tokens.back().is_type(Token.LPAR):
		var token:Token = tokens.pop_back();
		var condition = _parse_formula_parenthess(line, tokens);
		if !condition:
			condition = _parse_formula(line, tokens);
		if !condition:
			.print_err(line,'CONDITION pattern CONDITION invalid');
			return null;
		token = tokens.pop_back();
		if !token.is_type(Token.RPAR):
			.print_err(line,'CONDITION pattern RPAR %s invalid'%[token.text_get()])
			return null;
		return condition;
	return null;

static func _parse_formula(line:int, tokens:Array):
	var left = FunctionConfigParser._parse_variable(line, tokens);
	while tokens.size() and TokenStatic.is_all_operand(tokens.back().token_type_get()):
		left = _parse_operator(line, tokens, left);
	return left;

static func _parse_multiple(line, tokens:Array):
	if tokens.size() > 2 and tokens[tokens.size()-2].is_type(Token.MULTIPLE):
		var left = FunctionConfigParser._parse_variable(line, tokens);
		return _parse_operator(line, tokens, left);
	return _parse_operator(line, tokens);

static func _parse_operator(line:int, tokens:Array, vrbl = null):
	if tokens.back().is_type(Token.NOT):
		var token:Token = tokens.pop_back();
		var right = _parse_formula_parenthess(line, tokens);
		if !right:
			.print_err(line,'CONDITION pattern RIGHT OPERAND %s invalid'%[token.text_get()])
			return null;
		return Condition.new(token.token_type_get(),
							right);
	if !vrbl:
		vrbl = _parse_formula_parenthess(line, tokens);
		if !vrbl:
			vrbl = FunctionConfigParser._parse_variable(line, tokens);
		return vrbl
	var token:Token = tokens.pop_back();
	## не забть сделать валидацию возвращаемых типов
	if TokenStatic.is_comparsion_operand(token.token_type_get()):
		var right = _parse_formula_or_formula_parenthess(line, tokens);
		if !right:
			.print_err(line,'CONDITION pattern RIGHT OPERAND %s invalid'%[token.text_get()])
			return null;
		return ConditionBinarBit.new(
					token.token_type_get(),
					vrbl,
					right
			);
	elif TokenStatic.is_bit_operand(token.token_type_get()):
		var right = _parse_formula_or_formula_parenthess(line, tokens);
		if !right:
			.print_err(line,'CONDITION pattern RIGHT OPERAND %s invalid'%[token.text_get()])
			return null;
		return ConditionBinarBit.new(
					token.token_type_get(),
					vrbl,
					right
			);
	elif TokenStatic.is_calculate_operand(token.token_type_get()):
		var right = _parse_formula_parenthess(line, tokens);
		if !right:
			right = _parse_multiple(line, tokens);
		if !right:
			.print_err(line,'CONDITION pattern RIGHT OPERAND %s invalid'%[token.text_get()])
			return null;
		return ConditionBinarVar.new(
					token.token_type_get(),
					vrbl,
					right
			);
	.print_err(line,'CONDITION pattern OPERATOR %s invalid'%[token.text_get()])
	return null
