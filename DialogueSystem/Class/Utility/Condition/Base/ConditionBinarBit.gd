extends ConditionUnarBit

## Класс сравнения операндов. Нужен для работы условий в конфиге
## унарный оператор это оператор с одним операндом. Например !variable
class_name ConditionBinarBit

var _second_operand;
func _second_operand_set(value)->void:
	_second_operand = value;
func _second_operand_get():
	if (_second_operand is FunctionData) or (_second_operand is Condition):
		return _second_operand.exec();
	return _second_operand;

func _init(
		operator:int, 
		operand1,
		operand2
		).(
		operator,
		operand1
		)->void:
	self._second_operand_set(operand2);

func exec()->bool:
	match self.operator_get():
		TokenStatic.EQUAL: 
			print(self._first_operand_get())
			return self._first_operand_get() == self._second_operand_get();
		TokenStatic.NOT_EQUAL: 
			return self._first_operand_get() != self._second_operand_get();
		TokenStatic.LARGER:
			return self._first_operand_get() > self._second_operand_get();
		TokenStatic.LARGER_EQUAL:
			return self._first_operand_get() >= self._second_operand_get();
		TokenStatic.LESS:
			return self._first_operand_get() < self._second_operand_get();
		TokenStatic.LESS_EQUAL:
			return self._first_operand_get() <= self._second_operand_get();
		TokenStatic.AND:
			return self._first_operand_get() and self._second_operand_get();
		TokenStatic.OR:
			return self._first_operand_get() or self._second_operand_get();
	return true;
