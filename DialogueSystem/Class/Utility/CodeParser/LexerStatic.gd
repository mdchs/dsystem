extends Reference

class_name LexerStatic

static func load_config()->bool:
	_lexem_add(Lexem.new(TokenStatic.SPACE, "^[\\s\\t\\n\\r]", true));
	_lexem_add(Lexem.new(TokenStatic.VARIABLE,'^[$][A-Za-z0-9_]+'));
	_lexem_add(Lexem.new(TokenStatic.ANCHOR,'^([@#%&MRP][A-Za-z0-9_]+)|([$])'));
	_lexem_add(Lexem.new(TokenStatic.FALSE, "^(false|False|f|FALSE)"));
	_lexem_add(Lexem.new(TokenStatic.TRUE, "^(true|True|t|TRUE)"));
	_lexem_add(Lexem.new(TokenStatic.AND, "^(and|AND|And)"));
	_lexem_add(Lexem.new(TokenStatic.OR, "^(or|OR|Or)"));
	_lexem_add(Lexem.new(TokenStatic.NUMBER,"^[0-9]+"));
	_lexem_add(Lexem.new(TokenStatic.TEXT,"^[\"][^\"]*[\"]"));
	_lexem_add(Lexem.new(TokenStatic.NAME,"^[A-Za-z0-9_]+"));
	_lexem_add(Lexem.new(TokenStatic.LPAR,"^[(]"));
	_lexem_add(Lexem.new(TokenStatic.RPAR,"^[)]"));
	_lexem_add(Lexem.new(TokenStatic.EQUAL, "^=="));
	_lexem_add(Lexem.new(TokenStatic.NOT_EQUAL, "^!="));
	_lexem_add(Lexem.new(TokenStatic.LARGER_EQUAL, "^(>=|=>)"));
	_lexem_add(Lexem.new(TokenStatic.LESS_EQUAL, "^(<=|=<)"));
	_lexem_add(Lexem.new(TokenStatic.LARGER, "^[>]"));
	_lexem_add(Lexem.new(TokenStatic.LESS, "^[<]"));
	#_lexem_add(Lexem.new(TokenStatic.SEMICOLON,"^[;]"));
	#_lexem_add(Lexem.new(TokenStatic.COLON,"^[:]"));
	_lexem_add(Lexem.new(TokenStatic.PLUS,"^[+]"));
	_lexem_add(Lexem.new(TokenStatic.MINUS,"^[-]"));
	_lexem_add(Lexem.new(TokenStatic.MULTIPLE,"^[*]"));
	_lexem_add(Lexem.new(TokenStatic.ASSIGN,"^[=]"));
	_lexem_add(Lexem.new(TokenStatic.COMMA,"^[,]"));
	_lexem_add(Lexem.new(TokenStatic.DOT,"^[.]"));
	_lexem_add(Lexem.new(TokenStatic.NOT,"^[!]"));
	return true;

const _lexems:Array = [];
static func _lexem_add(lexem:Lexem)->void:
	_lexems.append(lexem);
static func lexems_get()->Array:
	return _lexems;

static func analyze(text:String, line:int)->Array:
	var positon:int = 0;
	var tokens:Array = [];
	while true:
		if !text.length():
			break;
		var is_err:bool = true;
		for lexem in lexems_get():
			var regmatch:RegExMatch = lexem.token_find(text);
			if !regmatch:
				continue;
			if !lexem.is_filtered_get():
				tokens.append(
						Token.new(
							lexem.token_type_get(),
							regmatch.get_string(),
							positon)
					);
			positon+=regmatch.get_end();
			text = text.substr(regmatch.get_end());
			is_err = false;
			break;
		if is_err:
			print_err(line, 'Unknown character expression in position %s'%[positon])
			return [];
	return tokens;


static func print_err(line:int, text:String)->void:
	print('Line ',line,': ',text);
