extends Reference

class_name TokenStatic

enum {
	PLUS,
	MINUS,
	ASSIGN,
	NAME,
	NUMBER,
	TEXT,
	COLON,
	SEMICOLON,
	SPACE,
	TAB,
	LPAR,
	RPAR,
	MULTIPLE,
	COMMA,
	EQUAL,
	NOT_EQUAL,
	LARGER,
	LESS,
	LARGER_EQUAL,
	LESS_EQUAL,
	FALSE,
	TRUE,
	AND,
	OR,
	NOT,
	ANCHOR,
	DOT,
	VARIABLE,
}

static func is_all_operand(token_type:int)->bool:
	return [PLUS, MINUS, MULTIPLE,
		LESS, LESS_EQUAL, EQUAL,
		NOT_EQUAL, LARGER, LARGER_EQUAL,
		AND, OR, NOT].has(token_type);

static func is_calculate_operand(token_type:int)->bool:
	return [PLUS, MINUS, MULTIPLE].has(token_type);

static func is_comparsion_operand(token_type:int)->bool:
	return [EQUAL,NOT_EQUAL,
			LESS, LESS_EQUAL,
			LARGER,LARGER_EQUAL].has(token_type);

static func is_bit_operand(token_type:int)->bool:
	return [AND, OR].has(token_type);

static func is_bit_const_operand(token_type:int)->bool:
	return [TRUE, FALSE].has(token_type);
