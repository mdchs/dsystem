extends Reference

class_name Lexem

var _token_type:int setget _token_type_set, token_type_get;
func _token_type_set(value:int)->void:
	_token_type = value;
func token_type_get()->int:
	return _token_type;

var _regex:RegEx setget ,regex_get;
func _regex_set(value:String)->void:
	_regex = RegEx.new();
	if _regex.compile(value) != OK:
		printerr('Lexem regex %s not load'%[value]);
func regex_get()->RegEx:
	return _regex;
func token_find(text:String)->RegExMatch:
	return self.regex_get().search(text);

var _is_filtered:bool setget _is_filtered_set, is_filtered_get;
func _is_filtered_set(value:bool)->void:
	_is_filtered = value;
func is_filtered_get()->bool:
	return _is_filtered;

func _init(token_type:int, regex:String, is_filtered:bool=false)->void:
	self._token_type_set(token_type);
	self._regex_set(regex);
	self._is_filtered_set(is_filtered);
