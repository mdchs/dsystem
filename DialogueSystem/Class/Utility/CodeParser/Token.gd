extends TokenStatic

class_name Token

var _token_type:int setget _token_type_set, token_type_get;
func _token_type_set(value:int)->void:
	_token_type = value;
func token_type_get()->int:
	return _token_type;
func is_type(token_type:int)->bool:
	return self.token_type_get() == token_type;

var _text:String setget _text_set, text_get;
func _text_set(value:String)->void:
	_text = value;
func text_get()->String:
	return _text;

var _position:int setget _position_set, position_get;
func _position_set(value:int)->void:
	_position = value;
func position_get()->int:
	return _position;

func _init(token_type:int, text:String, position:int)->void:
	self._token_type_set(token_type);
	self._text_set(text);
	self._position_set(position);
