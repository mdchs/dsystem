extends FunctionData

## Класс для функции по Якорю. Выполняются мгновенно.
## Выдают строковые представления при выполнении.
class_name FunctionAnchor

var _anchor setget _anchor_set, _anchor_get;
func _anchor_set(value)->void:
	_anchor = value;
func _anchor_get():
	return _anchor;
func _object_get()->Reference:
	var anchor = _anchor_get();
	if (anchor is FunctionData):
		anchor = anchor.exec();
	if (anchor is GameObject):
		return anchor;
	elif typeof(anchor) == TYPE_STRING:
		match anchor[0]:
			'@': return CharacterStatic.anchor_get(anchor);
			'#': return LocationStatic.anchor_get(anchor);
			'&': return ItemStatic.anchor_get(anchor);
			'%': return StuffStatic.anchor_get(anchor);
			'$': return GlobalObjectCalled;
	GlobalObject.print_err('ANCHOR %s not found in FUNCTION %s' %[anchor, self._fnc_name_get()])
	return null;

func _init(
		anchor, 
		fnc_name:String, 
		params:Array
		).(
		fnc_name,
		params
		)->void:
	self._anchor_set(anchor);

func exec(object = self._object_get()):
	var params:Array = [];
	for param in self._params_get():
		if (param is FunctionData):
			params.append(param.exec());
		params.append(param);
	return ._exec(object, params);
