extends Reference

## Класс функции хранящий основные свойства функции
class_name FunctionData

func _object_get()->Reference:
	return GameObject;

var _fnc_name:String setget _fnc_name_set, _fnc_name_get;
func _fnc_name_set(value:String)->void:
	_fnc_name = value;
func _fnc_name_get()->String:
	return _fnc_name;

var _params:Array setget _params_set, _params_get;
func _params_set(value:Array)->void:
	_params = value;
func _params_get()->Array:
	return _params;

func _init(fnc_name:String, params:Array)->void:
	self._fnc_name_set(fnc_name);
	self._params_set(params);

func is_active()->bool:
	if self._fnc_name_get():
		return true;
	return false;

func _exec(object=self._object_get(), params:Array = self._params_get()):
	return object.callv(self._fnc_name_get(), params);
