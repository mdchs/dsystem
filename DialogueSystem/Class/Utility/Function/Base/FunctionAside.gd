extends FunctionData

## Класс для функции выполненной с задержкой над игровым объектом 
## по ссылке. Задержка нужна для демонстрации "процесса работы", 
## т.к. функция выполняется мгновенно.
class_name FunctionAside

var _lifetime:int=-1 setget _lifetime_set, lifetime_get;
func _lifetime_set(value:int)->void:
	_lifetime = value+GlobalObject.time_get();
func lifetime_get()->int:
	return _lifetime;
func time_left()->int:
	return self.lifetime_get()-GlobalObject.time_get();

func _init(
		fnc_name:String='wait', 
		params:Array=[0],
		lifetime:int=0
		).(
		fnc_name,
		params)->void:
	self._lifetime_set(lifetime);

func reload(fnc_name:String,
			params:Array,
			lifetime:int
		)->void:
	self._init(fnc_name, 
				params,
				lifetime);

func exec(object:GameObject)->int:
# warning-ignore:return_value_discarded
	return int(._exec(object));
