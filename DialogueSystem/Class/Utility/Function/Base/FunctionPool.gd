extends Reference

## Класс хранящий пул функций для быстрой выдачи без фрагментаций памяти
class_name FunctionPool

const _functions:Array = [];
static func function_pool_load(start_size:int)->void:
	for _i in range(0,start_size):
		_function_pool_add(FunctionAside.new());
static func _function_pool_add(function:FunctionAside)->void:
	_functions.append(function);
static func _function_pool_get(fnc_name:String, 
								params:Array,
								lifetime:int)->FunctionAside:
	var function:FunctionAside = _functions.pop_back();
	if !function:
		return FunctionAside.new(fnc_name, 
								params, 
								lifetime);
	function.reload(fnc_name, 
					params, 
					lifetime);
	return function;
