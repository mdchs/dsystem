extends ConfigParser

## Класс парсинга функций.
class_name FunctionConfigParser


## после парсинга почистить бы это
const _chache:Dictionary = {};
static func _chache_add(pattern:String, value:FunctionAnchor)->void:
	_chache[hash(pattern)] = value;
static func _chache_get(pattern:String):
	return _chache.get(hash(pattern));

static func parse_function_list(line:int,text:String)->Array:
	var return_arr:Array = [];
	for elem in text.split(';',false):
		var fnc:FunctionAnchor = parse_function(line, elem);
		if !fnc:
			return [];
		return_arr.append(fnc);
	return return_arr;

static func parse_function(line:int, text:String)->FunctionAnchor:
	var fnc:FunctionAnchor = _chache_get(text);
	if fnc:
		return fnc;
	var tokens:Array = LexerStatic.analyze(text, line);
	if !tokens.size():
		return null;
	tokens.invert();
	fnc = _parse_function(line, tokens);
	if tokens.size() != 0:
		.print_err(line,'FUNCTION pattern not use all tokens');
		return null;
	if fnc:
		_chache_add(text, fnc);
	return fnc;

static func _parse_function(line:int, tokens:Array)->FunctionAnchor:
	if tokens.size() < 3:
		.print_err(line,'FUNCTION pattern invalid. End Line.');
		return null;
	var anchor;
	var token:Token;
	if tokens.back().is_type(Token.ANCHOR):
		token = tokens.pop_back();
		anchor = token.text_get();
	elif tokens.back().is_type(Token.VARIABLE):
		if tokens[tokens.size()-2].is_type(Token.ASSIGN):
			return _assign_variable(line, tokens);
		else:
			var fnc:FunctionAnchor = _parse_variable(line, tokens);
			if !fnc:
				return null;
			anchor = fnc;
	else:
		.print_err(line,'FUNCTION pattern ANCHOR %s invalid'%[token.text_get()])
		return null;
	token = tokens.pop_back();
	if !token.is_type(Token.DOT):
		.print_err(line,'FUNCTION pattern DOT %s invalid'%[token.text_get()])
		return null;
	token = tokens.pop_back();
	if !token.is_type(Token.NAME):
		.print_err(line,'FUNCTION pattern FUNCTION_NAME %s invalid'%[token.text_get()])
		return null;
	var func_name:String = token.text_get();
	var params:Array = [];
	if !_parse_params(line, tokens, params):
		return null;
	return FunctionAnchor.new(anchor, func_name, params);

static func _parse_params(line:int, tokens:Array, params:Array)->bool:
	var token:Token = tokens.pop_back();
	if !token.is_type(Token.LPAR):
		.print_err(line,'FUNCTION pattern LPAR %s invalid'%[token.text_get()])
		return false;
	while true:
		if !tokens.size():
			.print_err(line,'FUNCTION pattern RPAR invalid. End Line.');
			return false;
		elif tokens.back().is_type(Token.RPAR):
			token = tokens.pop_back();
			break;
		var param = _parse_variable(line, tokens);
		if param == null:
			return false;
		params.append(param);
		if !tokens.size():
			.print_err(line,'FUNCTION pattern RPAR invalid. End Line.');
			return false;
		token = tokens.pop_back();
		if token.is_type(Token.RPAR):
			break;
		elif !token.is_type(Token.COMMA):
			.print_err(line,'FUNCTION pattern COMMA %s invalid'%[token.text_get()])
			return false;
	if !token.is_type(Token.RPAR):
		.print_err(line,'FUNCTION pattern RPAR %s invalid'%[token.text_get()])
		return false;
	return true;

static func _parse_variable(line:int,tokens:Array):
	if !tokens.size():
		.print_err(line,'FUNCTION pattern PARAMETER invalid. End Line.');
		return null;
	var token:Token = tokens.pop_back();
	if token.is_type(Token.NAME):
		return token.text_get();
	elif token.is_type(Token.NUMBER):
		return int(token.text_get());
	elif token.is_type(Token.ANCHOR):
		if !tokens.size() or !tokens.back().is_type(Token.DOT):
			return token.text_get();
		tokens.append(token);
		var fnc:FunctionAnchor = _parse_function(line,tokens);
		if !fnc:
			.print_err(line,'FUNCTION pattern PARAMETER %s invalid'%[token.text_get()])
			return null;
		return fnc;
	elif token.is_type(Token.TRUE):
		return true;
	elif token.is_type(Token.FALSE):
		return false;
	elif token.is_type(Token.VARIABLE):
		var var_name:String = token.text_get();
		if !tokens.size() or tokens.back().is_type(Token.ASSIGN):
			.print_err(line,'FUNCTION pattern PARAMETER %s invalid'%[token.text_get()])
			return null;
		return FunctionAnchor.new('$','vget',[var_name]);
	elif token.is_type(Token.TEXT):
		return token.text_get().trim_prefix('"').trim_suffix('"');
	.print_err(line,'FUNCTION pattern VARIABLE %s invalid'%[token.text_get()])
	return null;

static func _assign_variable(line:int,tokens:Array):
	var token:Token = tokens.pop_back();
	if !token.is_type(Token.VARIABLE):
		.print_err(line,'ASSIGN pattern VARIABLE NAME %s invalid'%[token.text_get()])
		return null;
	var var_name:String = token.text_get();
	token = tokens.pop_back();
	if !token.is_type(Token.ASSIGN):
		.print_err(line,'ASSIGN pattern ASSIGN %s invalid'%[token.text_get()])
		return null;
	var variable = _parse_variable(line, tokens);
	if !variable:
		return null;
	return FunctionAnchor.new('$','vset',[var_name,variable]);
