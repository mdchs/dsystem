extends GOAPAction

## GOAPAction для крафта
class_name GOAPActionWork

static func exec(agent:GOAPAgent,
				mem:Dictionary,
				path:GOAPPath)->int:
	var item:ItemData = mem.get(agent.FIND_ITEM);
	if !item:
		return GOAPStatic.ERR;
	var item_count:int = mem.get(agent.FIND_ITEM_COUNT,0);
	if !item_count:
		return GOAPStatic.ERR;
	var prof:ProfessionData = mem.get(agent.FIND_PROFESSION);
	if !prof:
		if ._RECEIPT_NOT_FOUND(agent, 
								mem,
								path
							) != GOAPStatic.OK:
			return GOAPStatic.ERR;
		prof = mem.get(agent.FIND_PROFESSION);
	var receipt:Receipt = mem.get(agent.FIND_RECEIPT);
	if !receipt:
		if ._RECEIPT_NOT_FOUND(agent, 
								mem,
								path
							) != GOAPStatic.OK:
			return GOAPStatic.ERR;
		receipt = mem.get(agent.FIND_RECEIPT);
	if receipt.result_get() != item:
		return GOAPStatic.ERR;
	var inventory:Dictionary = agent.param_get(agent.INVENTORY,{});
# warning-ignore:narrowing_conversion
	var work_count:int = ceil(1.0*item_count/receipt.result_count_get());
	for reag in receipt.reagents_get().keys():
		var diff:int = inventory.get(reag, 0) - receipt.reagent_count_get(reag)*work_count;
		if diff < 0:
			if ITEM_NOT_FOUND(agent,
							reag,
							int(abs(diff)),
							path
						) != GOAPStatic.OK:
				return GOAPStatic.ERR;
	for reag in receipt.reagents_get().keys():
		inventory[item] = inventory.get(reag) - receipt.reagent_count_get(reag)*work_count;
	inventory[item] = inventory.get(item,0)+work_count*receipt.result_count_get(); ## количество
	path.weight_add(receipt.result_price_via_receipt());
	return GOAPStatic.OK;

static func ITEM_NOT_FOUND(agent:GOAPAgent,
						item:ItemData,
						count:int,
						path:GOAPPath)->int:
	return _ITEM_NOT_FOUND(agent,
						{
							agent.FIND_ITEM:item,
							agent.FIND_ITEM_COUNT:count
						},
						path
					);
	

static func name_get()->String:
	return 'GOAPActionWork';

static func is_abstract_get()->bool:
	return false;

static func convert_to_function(mem:Dictionary)->FunctionAside:
	var proffession:ProfessionData = mem.get(GOAPAgent.FIND_PROFESSION);
	var receipt:Receipt = mem.get(GOAPAgent.FIND_RECEIPT);
# warning-ignore:narrowing_conversion
	var work_count:int =  ceil(1.0*mem.get(GOAPAgent.FIND_ITEM_COUNT)/receipt.result_count_get());
	return FunctionPool._function_pool_get(
							'profession_work',
							[proffession, receipt, work_count],
							receipt.lifetime_get()*work_count);
