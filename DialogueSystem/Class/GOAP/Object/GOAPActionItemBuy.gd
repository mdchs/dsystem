extends GOAPAction

## GOAPAction для покупки предметов
class_name GOAPActionItemBuy

static func exec(agent:GOAPAgent,
				mem:Dictionary,
				path:GOAPPath)->int:
	var item:ItemData = mem.get(agent.FIND_ITEM);
	if !item:
		return GOAPStatic.ERR;
	var item_count:int = mem.get(agent.FIND_ITEM_COUNT,0);
	if StockExchangeStatistic.count_get(item) < item_count:
		return GOAPStatic.ERR;
	var price:int = item_count*StockExchangeStatic.min_price_get(item);
	var diff_money:int =  agent.param_get(agent.MONEY,0) - price;
	while diff_money < 0:
		if MONEY_NOT_FOUND(agent,
							diff_money,
							path
						) != GOAPStatic.OK:
			return GOAPStatic.ERR;
		diff_money =  agent.param_get(agent.MONEY,0) - price;
	var inventory:Dictionary = agent.param_get(agent.INVENTORY,{});
	inventory[item] = inventory.get(item,0)+item_count;
	agent.param_set(agent.MONEY, diff_money);
	path.weight_add(price);
	return GOAPStatic.OK;

static func MONEY_NOT_FOUND(agent:GOAPAgent,
							money:int,
							path:GOAPPath)->int:
	return  _MONEY_NOT_FOUND(agent,
							{agent.FIND_MONEY:money},
							path);

static func name_get()->String:
	return 'GOAPActionBuyItem';

static func is_abstract_get()->bool:
	return false;

static func convert_to_function(mem:Dictionary)->FunctionAside:
	var item:ItemData = mem.get(GOAPAgent.FIND_ITEM);
	var item_count:int = mem.get(GOAPAgent.FIND_ITEM_COUNT);
	return FunctionPool._function_pool_get(
							'item_buy',
							[item, item_count],
							1);
