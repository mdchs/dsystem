extends GOAPAction

## GOAPAction для выбора предмета для продажи
## где прибль должна быть самая низкая, но покрывать потребности
class_name GOAPActionSelectSaleItemByMinDiff

static func exec(agent:GOAPAgent,
				mem:Dictionary,
				_path:GOAPPath)->int:
	var inventory:Dictionary = agent.param_get(agent.INVENTORY,{});
	var find_money:int = agent.param_get(agent.FIND_MONEY,0);
	if !find_money:
		return GOAPStatic.ERR;
	var find_item:ItemData;
	var find_item_count:int;
	var find_item_price:int;
	var find_total:int;
	for item in inventory.keys():
		var item_price = agent.character_get().item_estimate(item)
		var item_count = inventory.get(item);
		var total:int = item_count*item_price;
		if (total < find_money):
			continue;
		if find_total and total > find_total:
			continue;
		find_item = item;
		find_item_count = item_count;
		find_item_price = item_price;
		find_total = find_item_count*find_item_price;
	if !find_item:
		return GOAPStatic.ERR;
	mem[agent.FIND_ITEM] = find_item;
	mem[agent.FIND_ITEM_COUNT] = find_item_count;
	mem[agent.FIND_ITEM_PRICE] = find_item_price;
	return GOAPStatic.OK;

static func name_get()->String:
	return 'GOAPActionSelectSaleItem';

static func is_abstract_get()->bool:
	return true;
