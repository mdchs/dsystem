extends GOAPAction

## GOAPAction для продажи предмета
class_name GOAPActionItemSell

static func exec(agent:GOAPAgent,
				mem:Dictionary,
				path:GOAPPath)->int:
	var find_money:int = agent.param_get(agent.FIND_MONEY,0);
	if !find_money:
		return GOAPStatic.ERR;
	var item:ItemData = mem.get(agent.FIND_ITEM);
	if !item:
		if ._SALE_ITEM_NOT_FOUND(agent, 
								mem,
								path
							) != GOAPStatic.OK:
			return GOAPStatic.ERR;
		item = mem.get(agent.FIND_ITEM);
	var item_price:int = mem.get(agent.FIND_ITEM_PRICE,0);
	if !item_price:
		if ._SALE_ITEM_NOT_FOUND(agent, 
								mem,
								path
							) != GOAPStatic.OK:
			return GOAPStatic.ERR;
		item_price = mem.get(agent.FIND_ITEM_PRICE);
	var item_count:int = mem.get(agent.FIND_ITEM_COUNT,0);
	if !item_count:
		if ._SALE_ITEM_NOT_FOUND(agent, 
								mem,
								path
							) != GOAPStatic.OK:
			return GOAPStatic.ERR;
		item_count = mem.get(agent.FIND_ITEM_COUNT,0);
	var inventory:Dictionary = agent.param_get(agent.INVENTORY,{});
	if inventory.get(item, 0) < item_count:
		return GOAPStatic.ERR;
	inventory[item] = inventory.get(item,0)-item_count;
	agent.param_set(agent.MONEY, item_price*item_count);
	return GOAPStatic.OK;

static func name_get()->String:
	return 'GOAPActionItemSell';

static func is_abstract_get()->bool:
	return false;

static func convert_to_function(mem:Dictionary)->FunctionAside:
	var item:ItemData = mem.get(GOAPAgent.FIND_ITEM);
	var item_count:int = mem.get(GOAPAgent.FIND_ITEM_COUNT);
	var price:int = mem.get(GOAPAgent.FIND_ITEM_PRICE);
	return FunctionPool._function_pool_get(
							'item_sale',
							[item, item_count, price],
							1);
