extends GOAPAction

## GOAPAction для выбора рецепта
class_name GOAPActionSelectReceipt

static func exec(agent:GOAPAgent,
				mem:Dictionary,
				_path:GOAPPath)->int:
	var item:ItemData = mem.get(agent.FIND_ITEM);
	if !item:
		return GOAPStatic.ERR;
	var proffesions:Dictionary = agent.param_get(agent.PROFESSIONS,{});
	for proffesion in proffesions.keys():
		for receipt in proffesion.receipts_get():
			if (receipt as Receipt).result_get() != item:
				continue;
			if (receipt as Receipt).need_exp_get() < proffesions[proffesion]:
				continue;
			mem[agent.FIND_RECEIPT] = receipt;
			mem[agent.FIND_PROFESSION] = proffesion;
			return GOAPStatic.OK;
	return GOAPStatic.ERR;

static func name_get()->String:
	return 'GOAPActionSelectReceipt';

static func is_abstract_get()->bool:
	return true;
