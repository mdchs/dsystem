extends GOAPAction

## GOAPAction для ожидания
class_name GOAPActionWait

static func name_get()->String:
	return 'GOAPActionWait';

static func is_abstract_get()->bool:
	return false;


static func convert_to_function(_mem:Dictionary)->FunctionAside:
	return FunctionPool._function_pool_get(
							'wait',
							[],
							1);
