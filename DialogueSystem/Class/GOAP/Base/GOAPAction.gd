extends GOAPStatic

## Базовый класс действия и его расчета при планировании
## Использует GOAPAgent для сохранения данных между действиями
class_name GOAPAction


## Базовая функция вызова расчета действия
## Возвращает статус действия
static func exec(_agent:GOAPAgent, 
				_mem:Dictionary,
				_path:GOAPPath)->int:
	return GOAPStatic.OK;

## Получить название класса для принта в Лог,
## т.к. годот не поддерживает названия кастомных классов
static func name_get()->String:
	return 'GOAPAction';

#№ Абстрактный класс не создает новый GOAPAgent, 
## а перезаписывает данные в старом
## Не создают функции при конвертации и не записываются в GOAPPath
static func is_abstract_get()->bool:
	return true;

## Обсчет вызова других GOAPAction из текущего,
## когда для выполнения действия требуется другое действие.
static func action_exec(agent:GOAPAgent,
						result:int,
						mem:Dictionary,
						path:GOAPPath)->int:
	var transits:Dictionary = .transit_get_by_result(result);
	for taction in transits.keys():
		var action_weight:int = transits[taction];
		var save_path_weight:int =  path.weight_get();
		if save_path_weight + action_weight > path.weight_max_get():
			continue;
		var save_path_size:int = path.path_size_get();
		var agent_new:GOAPAgent = agent;
		if !taction.is_abstract_get():
			agent_new = ._gagent_pool_get_by_agent(agent);
		if taction.exec(agent_new, mem, path) == GOAPStatic.OK:
			if !taction.is_abstract_get():
				path.path_add(mem,
							taction,
							action_weight);
				agent.agent_reload(agent_new);
				._gagent_pool_add(agent_new);
			else:
				path.weight_add(action_weight);
			return GOAPStatic.OK;
		else:
			path.resize(save_path_size);
			path._weight_set(save_path_weight);
	return GOAPStatic.ERR;

## Функция для удобства, когда рецепт не найден
static func _RECEIPT_NOT_FOUND(agent:GOAPAgent, 
								mem:Dictionary,
								path:GOAPPath)->int:
	return action_exec(agent,
						GOAPStatic.RECEIPT_NOT_FOUND,
						mem,
						path);

## Функция для удобства, когда предмет не найден
static func _ITEM_NOT_FOUND(agent:GOAPAgent, 
							mem:Dictionary,
							path:GOAPPath)->int:
	return action_exec(agent,
						GOAPStatic.ITEM_NOT_FOUND,
						mem,
						path);

## Функция для удобства, когда денежных средств не хватает
static func _MONEY_NOT_FOUND(agent:GOAPAgent, 
							mem:Dictionary,
							path:GOAPPath)->int:
	return action_exec(agent,
						GOAPStatic.MONEY_NOT_FOUND,
						mem,
						path);

## Функция для удобства, когда не найден предмет, который можно продать
static func _SALE_ITEM_NOT_FOUND(agent:GOAPAgent, 
							mem:Dictionary,
							path:GOAPPath)->int:
	return action_exec(agent,
						GOAPStatic.SALE_ITEM_NOT_FOUND,
						mem,
						path);

## Создает с помощью GOAPAction в FunctionAside, 
## который исполняет код
static func convert_to_function(_mem:Dictionary)->FunctionAside:
	return null;
