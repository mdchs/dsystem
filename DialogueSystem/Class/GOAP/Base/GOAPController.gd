extends GOAPStatic

class_name GOAPController

## максимальный вес плана
const _MAX_PLAN_WEIGHT:int = 1000;

## Функция поиска плана действий
static func GOAPFind(character:CharacterData,
					object:GameObject,
					count:int)->Array:
	var plan:Array = [];
	## максимальный вес плана
	var weight_max:int = _MAX_PLAN_WEIGHT;
	var transits:Dictionary = .transit_get_by_object(object);
	for action in transits.keys():
		var agent:GOAPAgent = ._gagent_pool_get_by_character(character);
		var mem:Dictionary = agent.mem_get_by_object(
								object, 
								count);
		var path:GOAPPath = ._gpath_pool_get(weight_max);
		var result:int = action.exec(agent,
									mem,
									path);
		._gagent_pool_add(agent);
		if result != GOAPAction.OK:
			continue;
		path.path_add(mem, 
					action, 
					transits[action]);
		weight_max = path.weight_get();
		plan.append(path);
	
	if !plan.size():
		return [];
	var min_path:GOAPPath = _min_goap_path_get(plan);
	plan = min_path.convert_to_functions();
	._gpath_pool_add(min_path);
	return plan;

## Поиск минимального пути по суммарному весу пути.
static func _min_goap_path_get(plan:Array)->GOAPPath:
	var min_path:GOAPPath = plan.front();
	for path_id in range(1,plan.size()):
		if min_path.weight_get() > plan[path_id].weight_get():
			._gpath_pool_add(min_path);
			min_path = plan[path_id];
	return min_path;
