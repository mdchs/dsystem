extends Reference

## Хранилище данных для планирования действий
class_name GOAPAgent

## Параметры для оптимизации доступа к информации класса
enum {
	INVENTORY, ## инвентарь
	LOCATION, ## локации
	PROFESSIONS, ## профессии
	MONEY,  ## деньги
	FIND_ITEM, ## искомый предмет
	FIND_ITEM_COUNT, ## количество искомого предмета
	FIND_ITEM_PRICE, ## цена искомого предмета
	FIND_RECEIPT, ## искомый рецепт 
	FIND_PROFESSION, ## искомая профессия
	FIND_MONEY,  ## искомое количество денег
}

 ## Получение паттерна поиска по объекту
static func mem_get_by_object(object:GameObject, 
							count:int)->Dictionary:
	if object is ItemLogic:
		return {FIND_ITEM:object,
				FIND_ITEM_COUNT:count}
	elif object == ItemStatic.money_item_get():
		return {FIND_MONEY:count}
	return {};

## Словарь для сохранения информации
var _params:Dictionary = {} setget _params_set,params_get;
func _params_set(value:Dictionary)->void:
	_params = value.duplicate(true);
func params_get()->Dictionary:
	return _params;
func param_set(param:int, value)->void:
	_params[param] = value;
func param_get(param:int, default=null):
	return _params.get(param,default);

## Перегрузка агента другим Агентом для пула объектов
func agent_reload(parent:GOAPAgent)->void:
	self._params_set(parent.params_get());

## Перегрузка агента Персонажем для пула объектов
func character_reload(character:CharacterData)->void:
	self.param_set(INVENTORY, character.inventory_get().get_all().duplicate());
	self.param_set(MONEY, character.money_get());
	self.param_set(PROFESSIONS, character.profs_get().get_all().duplicate())
	self.param_set(LOCATION, character.location_get());

func _init(character:CharacterData = null)->void:
	if !character:
		return;
	self.character_reload(character);
