extends GOAPStatic

class_name GOAPConfigLoader

## Ручками не забываем самые дешевые делать меньшими элементами
## не ручками пока лень

## Загрузка конфига
static func load_config()->bool:
	.transit_set(
		## Cтатус GOAPAction
		{
			GOAPAction.RECEIPT_NOT_FOUND:{
				## GOAPAction:Weight
				GOAPActionSelectReceipt:5
			},
			GOAPAction.ITEM_NOT_FOUND:{
				GOAPActionItemBuy:40,
				GOAPActionWork:70,
			},
			GOAPAction.MONEY_NOT_FOUND:{
				GOAPActionItemSell:50,
			},
			GOAPAction.SALE_ITEM_NOT_FOUND:{
				GOAPActionSelectSaleItemByMinDiff:5,
			}
		}
	);
	return true;
