extends Reference

class_name GOAPStatic


## Все статусы действий
enum {
	OK,
	ERR,
	ITEM_NOT_FOUND,
	RECEIPT_NOT_FOUND,
	MONEY_NOT_FOUND,
	SALE_ITEM_NOT_FOUND,
}

## Словарь переходов между действиями
const _TRANSIT:Dictionary = {
}

static func transit_set(value:Dictionary)->void:
	for key in value.keys():
		transit_add(key, value[key]);
static func transit_get()->Dictionary:
	return _TRANSIT;
static func transit_add(param,
						value)->void:
	_TRANSIT[param] = value;
static func transit_get_by_result(result:int)->Dictionary:
	return _TRANSIT.get(result,{});
static func transit_get_by_object(value:GameObject)->Dictionary:
	if ItemStatic.money_item_get() == value:
		return transit_get_by_result(MONEY_NOT_FOUND);
	return transit_get_by_result(ITEM_NOT_FOUND);

##Пул объектов GOAPPath
const _gpath_pool:Array = [];
static func gpath_pool_load(start_size:int)->void:
	for _i in range(0,start_size):
		_gpath_pool.append(GOAPPath.new(0));
static func _gpath_pool_add(gpath:GOAPPath)->void:
	_gpath_pool.append(gpath);
static func _gpath_pool_get(weight_max:int)->GOAPPath:
	var gpath:GOAPPath = _gpath_pool.pop_back();
	if !gpath:
		return GOAPPath.new(weight_max);
	gpath.reload(weight_max)
	return gpath;

##Пул объектов GOAPAgent
const _gagent_pool:Array = [];
static func gagent_pool_load(start_size:int)->void:
	for _i in range(0,start_size):
		_gagent_pool.append(GOAPAgent.new());
static func _gagent_pool_add(gagent:GOAPAgent)->void:
	_gagent_pool.append(gagent);
static func _gagent_pool_get_by_character(character:CharacterData)->GOAPAgent:
	var gagent:GOAPAgent = _gagent_pool.pop_back();
	if !gagent:
		return GOAPAgent.new(character);
	gagent.character_reload(character);
	return gagent;
static func _gagent_pool_get_by_agent(agent:GOAPAgent)->GOAPAgent:
	var gagent:GOAPAgent = _gagent_pool.pop_back();
	if !gagent:
		gagent = GOAPAgent.new();
	gagent.agent_reload(agent);
	return gagent;
