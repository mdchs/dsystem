extends Reference

## Объект маршрута. Сохраняет маршрут GOAPAction, который прошел GOAPAgent
class_name GOAPPath

## Суммарный вес маршрута
var _weight:int setget _weight_set, weight_get;
func _weight_set(value:int)->void:
	_weight = value;
func weight_get()->int:
	return _weight;
func weight_add(value:int)->void:
	_weight += value;

## Максимально допустимый вес для маршрута
## Есть ли в нем необходимость или лучше передавать константой?
var _weight_max:int setget _weight_max_set, weight_max_get;
func _weight_max_set(value:int)->void:
	_weight_max = value;
func weight_max_get()->int:
	return _weight_max;

## Пусть из Action, для которого в соседнем массиве сохранена Память
var _path_mem:Array = [];
var _path_actions:Array = [];
func path_add(mem:Dictionary,
			action:GDScript,
			weight:int)->void:
	_path_mem.append(mem);
	_path_actions.append(action);
	self.weight_add(weight);
func path_size_get()->int:
	return _path_mem.size();
func mem_get(node_id:int)->Dictionary:
	return _path_mem[node_id];
func action_get(node_id:int)->GDScript:
	return _path_actions[node_id];
func resize(value:int)->void:
	_path_mem.resize(value);
	_path_actions.resize(value);

func _init(weight_max:int)->void:
	self._weight_max_set(weight_max);

## Перегрузка для пула объектов
func reload(weight_max:int)->void:
	self._weight_max_set(weight_max);
	self._weight_set(0);
	_path_mem.clear();
	_path_actions.clear();

## Конвертация маршрута в набор функций-действий
func convert_to_functions()->Array:
	var plan:Array = [];
	for node_id in self.path_size_get():
		var action:GDScript = self.action_get(node_id);
		plan.append(action.create_func(self.mem_get(node_id)));
	return plan;
