extends Reference

## Статический класс для сохранения констант или переменных
class_name TaskTrackerStatic

const _tasks_pool:Array = [];
static func task_pool_load(start_size:int)->void:
	for _i in range(0,start_size):
		_task_pool_add(Task.new());
static func _task_pool_add(task:Task)->void:
	_tasks_pool.append(task);
static func _tasks_pool_get(need:Need)->Task:
	var task:Task = _tasks_pool.pop_back();
	if !task:
		return Task.new(need);
	task.reload(need);
	return task;
