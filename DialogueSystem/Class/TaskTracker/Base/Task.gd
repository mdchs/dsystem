extends Reference

## Задача по обработке нужды персонажа
class_name Task

## Статусы по выполнению работы с задачей
enum {
	NO_PLAN,
	NEED_TIMEOUT,
	NO_OBJECT,
	FUNC_EXEC_ERR,
	FUNC_EXEC_OK,
	WORK,
}

var _need:Need setget _need_set, need_get;
func _need_set(value:Need)->void:
	_need = value;
func need_get()->Need:
	return _need;

var _plan:Array=[] setget _plan_set, _plan_get;
func plan_create(character:CharacterData)->bool:
	var plan:Array = GOAPController.GOAPFind(
							character, 
							self.need_get().object_get(),
							self.need_get().count_get());
	if !plan.size():
		return false;
	self._plan_set(plan);
	return true;
func _plan_set(value:Array)->void:
	_plan = value;
func _plan_get()->Array:
	return _plan;
func _plan_front_action_drop(fnc:FunctionAside)->void:
	_plan.resize(_plan.size()-1);
	FunctionPool._function_pool_add(fnc);
	fnc = self.get_first_action();
	if !fnc:
		return;
	fnc._lifetime_set(fnc.lifetime_get());
func get_first_action()->FunctionAside:
	if !_plan.size():
		return null;
	return _plan.back();

func time_left()->int:
	var fnc:FunctionAside = self.get_first_action();
	if !fnc:
		return 0;
	return fnc.time_left();

func exec(character:CharacterData)->int:
	if self.need_get().time_left() < 0:
		return NEED_TIMEOUT;
	if !self.need_get().object_get():
		return NO_OBJECT;
	if !self._plan_get().size():
		return NO_PLAN;
	var fnc:FunctionAside = self.get_first_action();
	if self.time_left():
		return WORK;
	var result:int = fnc.exec(character);
	self._plan_front_action_drop(fnc);
	if result:
		if self._plan_get().size():
			return WORK;
		return FUNC_EXEC_OK;
	return FUNC_EXEC_ERR;

func _init(need:Need=null)->void:
	self.reload(need);

func reload(need:Need)->void:
	self._need_set(need);
