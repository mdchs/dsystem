extends Reference

## Класс работы с трекерами тасков по каждому персонажу
class_name TaskTrackerController
## Массив треков. Основной принцип работы - постоянная сортировка массива
## после выполнения действий. Это нужно для того, чтобы обходить не все элементы
## а только тех, кто действительно хочет выоплнить действие

static func is_active()->bool:
	return  Config.TASK_CONTROLLER_GET() == Config.CONTROLLERS.GOAP;

const _trackers:Array = [];
static func _tracker_add(character:CharacterData)->void:
	if !is_active():
		return;
	_trackers.append(TaskTracker.new(character));
static func _trakers_get()->Array:
	return _trackers;
static func _traker_sort()->void:
	_trackers.sort_custom(TaskTracker,'sort_max');

static func autojob()->void:
	if !is_active():
		return;
	var count:int = 0;
	for tracker_id in _trakers_get().size():
		var tracker:TaskTracker = _trakers_get()[tracker_id];
		var last_time:int = tracker.time_left();
		if last_time < 0:
			var character:CharacterData = tracker.character_get();
			var need:Need = SoulsContoller.need_get(character);
			if !need:
				break;
			tracker.task_create(need);
			GlobalObject.println('task CREATE');
		elif last_time == 0:
			var character:CharacterData = tracker.character_get();
			var need:Need = tracker.first_need_get();
			match tracker.exec():
				TaskTracker.TASK_EXEC_OK:
					SoulsContoller.need_success(character, need);
					tracker.first_task_drop();
					GlobalObject.println('task OK');
				TaskTracker.TASK_EXEC_ERR:
					SoulsContoller.need_problem(character, need);
					GlobalObject.println('task ERR');
				TaskTracker.NO_PLAN:
					if !tracker.first_task_create_plan():
						SoulsContoller.need_failed(character, need);
						tracker.first_task_drop();
						GlobalObject.println('task PLAN FAIL');
					else:
						GlobalObject.println('task PLAN');
				TaskTracker.NEED_TIMEOUT:
					SoulsContoller.need_failed(character, need);
					tracker.first_task_drop();
					GlobalObject.println('task FAIL');
				TaskTracker.NO_OBJECT:
					if !need.object_find():
						SoulsContoller.need_failed(character, need);
						tracker.first_task_drop();
						GlobalObject.println('task OBJECT FAIL');
					else:
						GlobalObject.println('task FIND OBJECT');
		if last_time > 0:
			break;
		count+=1;
	if !count:
		return;
	_traker_sort();
