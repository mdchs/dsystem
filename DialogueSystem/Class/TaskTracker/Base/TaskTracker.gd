extends TaskTrackerStatic

## Трекер Тасков для Персонажа по работе с множеством Тасков
class_name TaskTracker

enum {
	NO_TASK,
	NEED_TIMEOUT,
	NO_PLAN,
	NO_OBJECT,
	TASK_EXEC_ERR,
	TASK_EXEC_OK,
	WORK,
}

var _character:CharacterData setget _character_set, character_get;
func _character_set(value:CharacterData)->void:
	_character = value;
func character_get()->CharacterData:
	return _character;

var _tasks:Array;
func task_create(need:Need)->void:
	var task:Task = TaskTrackerStatic._tasks_pool_get(need);
	self._task_add(task);
func _task_add(task:Task)->void:
	_tasks.push_front(task);
func _first_task_get()->Task:
	if !_tasks.size():
		return null;
	return _tasks.back();
func first_task_drop()->void:
	var task:Task = self._first_task_get();
	if !task:
		return;
	_tasks.resize(_tasks.size()-1);
	._task_pool_add(task);
func first_task_create_plan()->bool:
	var task:Task = self._first_task_get();
	if !task:
		return false;
	return task.plan_create(self.character_get());
func first_need_get()->Need:
	var task:Task = self._first_task_get();
	if !task:
		return null;
	return task.need_get();
func time_left()->int:
	var task:Task = self._first_task_get();
	if !task:
		return -1;
	return task.time_left();

func exec()->int:
	var task:Task = self._first_task_get();
	if !task:
		return NO_TASK;
	var result:int = task.exec(self.character_get());
	if result == Task.FUNC_EXEC_OK:
		return TASK_EXEC_OK;
	elif result == Task.FUNC_EXEC_ERR:
		return TASK_EXEC_ERR;
	elif result == Task.NEED_TIMEOUT:
		return NEED_TIMEOUT;
	elif result == Task.NO_OBJECT:
		return NO_OBJECT;
	elif result == Task.NO_PLAN:
		return NO_PLAN;
	return WORK;

static func sort_max(a:TaskTracker, b:TaskTracker)->bool:
	if a.time_left() > b.time_left():
		return true;
	return false;

func _init(character:CharacterData)->void:
	self._character_set(character);
