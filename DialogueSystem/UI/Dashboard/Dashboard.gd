extends Control

signal transitions_show(location);
signal inventory_show(inventory);

var _has_show:bool = true setget has_show_set, has_show_get;
func has_show_set(value:bool)->void:
	_has_show = value;
	if !self.has_show_get():
		self.dashboard_hide();
func has_show_get()->bool:
	return _has_show;

func dashboard_show()->void:
	if !self.has_show_get():
		return;
	self.show();

func dashboard_hide()->void:
	self.hide();

func _on_Transitions_pressed()->void:
	self.emit_signal('transitions_show', 
			CharacterStatic.player_get().location_get());

func _on_Inventory_pressed()->void:
	self.emit_signal('inventory_show', 
			CharacterStatic.player_get().inventory_get());

func _ready():
	GlobalObject.dashboard_set(self);
