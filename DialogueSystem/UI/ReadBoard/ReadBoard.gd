extends Control

signal readboard_hide;

func readboard_show(nme:String, text:String)->void:
	$Panel/Header.text = nme;
	$Panel/Text.text = text;
	self.show();
func readboard_hide()->void:
	self.hide();

func _on_ExitButton_pressed():
	self.emit_signal('readboard_hide');

func _ready()->void:
	GlobalObject.readboard_set(self);
