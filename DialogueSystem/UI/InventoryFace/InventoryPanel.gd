extends Container

export var X_SIZE_ITEM:int = 7;

func _item_count_show(idx:int, count:int)->void:
	$labels.get_child(idx).text = str(count);
	$labels.get_child(idx).show();

func _items_hide()->void:
	for label in $labels.get_children():
		label.hide();

func inventory_show(inventory:Inventory)->void:
	var x:int = 0;
	var y:int = 0;
	for item in inventory.items_get():
		$ItemMap.set_cell(x,y,false,false,false,false,item.image_get());
		self._item_count_show(
					y*7+x,
					inventory.count_get(item)
			);
		if x + 1 < X_SIZE_ITEM:
			x+=1;
		else:
			x=0;
			y+=1;
		

func inventory_clear()->void:
	$ItemMap.clear();
	self._items_hide();


func _ready()->void:
	pass
