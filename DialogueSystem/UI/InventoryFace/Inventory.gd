extends Control

signal inventory_hide

var _inventory:Inventory setget _inventory_set, _inventory_get
func _inventory_set(value:Inventory)->void:
	_inventory = value;
	$Panel/Items.inventory_clear();
	$Panel/Items.inventory_show(_inventory);
func _inventory_get()->Inventory:
	return _inventory;

func inventory_show(inventory:Inventory)->void:
	self._inventory_set(inventory);
	self.show();
func inventory_hide()->void:
	self.hide();

func _ready():
	pass

func _on_ExitButton_pressed()->void:
	self.emit_signal('inventory_hide');
