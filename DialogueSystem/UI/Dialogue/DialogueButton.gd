extends Button

class_name DialogueButton

signal message_selected(messages);

var _message:MessageData setget _message_set, message_get;
func _message_set(value:MessageData)->void:
	_message = value;
	self.text = _message.text_get();
func message_get()->MessageData:
	return _message;

func _on_DialogueButton_pressed():
	for action in self.message_get().actions_get():
# warning-ignore:return_value_discarded
		action.exec();
	self.emit_signal("message_selected",self.message_get().childs_get());

func init(message:MessageData)->void:
	self._message_set(message);
