extends Control

signal dialogue_end;

const DIALOGUE_BUTTON_PCK = preload("res://UI/Dialogue/DialogueButton.tscn");

var _BOT_NODE:Control;
var _PLAYER_NODE:Control;

var _imessage:MessageData setget _imessage_set, imessage_get;
func _imessage_set(value:MessageData)->void:
	_imessage = value;
	if _imessage:
		self._bot_message_show(_imessage);
func imessage_get()->MessageData:
	return _imessage;

func dialogue_show(interlocutor:CharacterData)->void:
	CharacterData._anchor_add(Config.INTERLOCUTOR_ANCHOR,
								interlocutor);
	$MainPanel/CharacterImage.init(interlocutor);
	self.show();
	$MainPanel/Split/TopPanel.text = interlocutor.name_get();
	self._dialogue_load(interlocutor.messages_get());

func dialogue_hide()->void:
	self.hide();

func dialogue_end()->void:
	self.emit_signal("dialogue_end");

func _on_DialogueContinue_pressed()->void:
	for action in self.imessage_get().actions_get():
		action.exec();
	self._dialogue_load(self.imessage_get().childs_get());

func _dialogue_load(messages:Array)->void:
	if !messages.size():
		messages = CharacterData.interlocutor_get().messages_get();
		if messages.size():
			self._dialogue_load(messages);
		else:
			self.dialogue_end();
	else:
		var message = messages.front();
		if message.is_empty() and message.childs_get().size():
			for action in message.actions_get():
				action.exec();
			self._dialogue_load(message.childs_get());
		elif !message.is_choise_get():
			self._imessage_set(message);
		else:
			self._player_message_show(messages);

func _bot_message_show(message:MessageData)->void:
	_BOT_NODE.get_node('Text').text = message.text_get();
	_BOT_NODE.show();
	_PLAYER_NODE.hide();

func _player_message_show(messages:Array)->void:
	self._player_choise_clear();
	for message in messages:
		if !message.is_check():
			continue;
		self._player_choise_add(message);
	_PLAYER_NODE.show();
	_BOT_NODE.hide();

func _player_choise_clear()->void:
	var list:GridContainer = _PLAYER_NODE.get_node('Scroll/List');
	for child in list.get_children():
		child.queue_free();

func _player_choise_add(message:MessageData)->void:
	var list:GridContainer = _PLAYER_NODE.get_node('Scroll/List');
	var button:DialogueButton = DIALOGUE_BUTTON_PCK.instance();
	list.add_child(button);
	button.init(message);
	if button.connect("message_selected",self,"_dialogue_load") != OK:
		print('not connect player answer');

func _ready()->void:
	GlobalObject.dialogue_set(self);
	_BOT_NODE = $MainPanel/Split/Body/Bot;
	_PLAYER_NODE = $MainPanel/Split/Body/Player;
