extends Node2D

class_name PointNode

const CHARACTER_IMAGE_PCKG = preload("res://UI/GameObjects/Image/CharacterImage.tscn");
const STUFF_IMAGE_PCKG = preload("res://UI/GameObjects/Image/StuffImage.tscn");

signal object_select(object);

var _point:Point setget _point_set, point_get;
func _point_set(value:Point)->void:
	_point = value;
	self.position = _point.position_get();
	self.scale.x = _point.scale_get();
	self.scale.y = _point.scale_get();
	self._stuff_load(_point.stuff_get());
func point_get()->Point:
	return _point;
func is_interaction_set(value:bool, filter:GDScript = null)->void:
	for child in self.get_children():
		if filter and child.get_script() != filter:
			continue;
		child.is_interaction_set(value);
func is_show_set(value:bool)->void:
	for child in self.get_children():
		child.visible = value;
func is_outline_visible_set(value:bool, filter:GDScript = null)->void:
	for child in self.get_children():
		if filter and child.get_script() != filter:
			continue;
		child.is_outline_visible_set(value);

func _stuff_load(stuff:StuffData)->void:
	if !stuff:
		return;
	var stuff_image:StuffImage = STUFF_IMAGE_PCKG.instance();
	self.add_child(stuff_image);
	stuff_image.init(stuff);
	if stuff_image.connect("object_select",self,"object_select") != OK:
		print('image not connected to point');

func is_engaged()->bool:
	if self.character_get():
		return true;
	return self.point_get().is_engaged();

func character_load(character:CharacterData)->void:
	if !character:
		return;
	var character_image:CharacterImage = CHARACTER_IMAGE_PCKG.instance();
	self.add_child(character_image);
	var animation:String = '';
	if self.point_get().stuff_get():
		animation = self.point_get().stuff_get().animation_get();
	character_image.init(character, animation);
	if character_image.connect("LocationExit", self, "character_location_exit") != OK:
		printerr('CharacterImage not connected Point');
	if character_image.connect("object_select",self,"object_select") != OK:
		printerr('CharacterImage not connected Point');

func clear()->void:
	for child in self.get_children():
		child.queue_free();

func object_select(object:GameObject)->void:
	self.emit_signal("object_select", object);

func character_get()->CharacterImage:
	for child in self.get_children():
		if child is CharacterImage:
			return child;
	return null;

func init(point:Point)->void:
	self.clear();
	self._point_set(point);

func character_location_exit(character_image:CharacterImage)->void:
	character_image.queue_free();
