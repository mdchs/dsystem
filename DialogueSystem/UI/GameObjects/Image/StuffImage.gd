extends FaceImage

class_name StuffImage

func is_interaction_set(value:bool)->void:
	if !self.stuff_get().is_interaction_get() and !self.is_interaction_get():
		return;
	.is_interaction_set(value);
	$Image.is_interaction_set(_is_interaction);

var _stuff:StuffData setget _stuff_set, stuff_get;
func _stuff_set(value:StuffData)->void:
	_stuff = value;
	self.is_interaction_set(_stuff.is_interaction_get());
func stuff_get()->StuffData:
	return _stuff;

func position_set(mode_flag:int)->void:
	self.position = $Image.texture.get_size()*_POSITIONS[mode_flag];

func init(stuff:StuffData, mode_flag:int = FLOOR)->void:
	self._stuff_set(stuff);
	$Image.texture = load(Config.STUFF_IMAGE_PATH+stuff.image_get());
	self.position_set(mode_flag);

func _on_Image_mouse_click(_event:InputEvent)->void:
	self.stuff_get().actions_exec(CharacterStatic.player_get());
	self.emit_signal('object_select', self.stuff_get());

func is_outline_visible_set(value:bool)->void:
	$Image.is_show_outline_set(value);
