extends Node2D

class_name LocationImage

const POINTS_NODE_PCKG = preload("res://UI/GameObjects/Navigation/PointNode.tscn");

signal object_select(object);

var _location:LocationData setget _location_set, location_get;
func _location_set(value:LocationData)->void:
	if _location:
		_location.disconnect("LocationEnter",self,"_character_add_point");
	_location = value;
	if _location.connect("LocationEnter", self, "_character_add_point") != OK:
		printerr('LocationEnter not connected LocationImage');
	self._image_load(_location.image_get());
func location_get()->LocationData:
	return _location;

func _image_load(image_name:String)->void:
	$Color.texture = load(Config.LOCATION_IMAGE_PATH+image_name);
	$Light.texture = load(Config.LOCATION_IMAGE_PATH+'L'+image_name);

func _points_load(location:LocationData)->void:
	self._points_clear();
	var points:Array = location.points_get();
	var characters:Array = location.characters_get();
	var character_id:int = -1;
	for point in points:
		var point_node:PointNode = POINTS_NODE_PCKG.instance();
		$Points.add_child(point_node);
		point_node.init(point);
		if point_node.connect("object_select",self,"object_select") != OK:
			print('point not connect select object');
		if point_node.is_engaged():
			continue;
		while characters.size() > character_id+1:
			character_id+=1;
			if characters[character_id] != CharacterStatic.player_get():
				point_node.character_load(characters[character_id]);
				break;
func _character_add_point(character:CharacterData)->void:
	for point in $Points.get_children():
		if point.is_engaged():
			continue;
		point.character_load(character);
		return;

func _points_clear()->void:
	for point in $Points.get_children():
		point.queue_free();
func is_interaction_set(value:bool,filter:GDScript = null)->void:
	for point in $Points.get_children():
		point.is_interaction_set(value,filter);
func is_outline_visible_set(value:bool,filter:GDScript = null)->void:
	for point in $Points.get_children():
		point.is_outline_visible_set(value,filter);
func IS_INTERACTION_SET_STUFF(value:bool)->void:
	self.is_interaction_set(value, StuffImage);
func is_show_set(value:bool)->void:
	for point in $Points.get_children():
		point.is_show_set(value);

func object_select(object:GameObject)->void:
	self.is_interaction_set(false);
	self.emit_signal("object_select", object);

func restore_location()->void:
	self.is_interaction_set(true);
	self.is_show_set(true);

func init(location:LocationData)->void:
	self._location_set(location);
	self._points_load(location);

func _ready()->void:
	GlobalObject.place_set(self);
