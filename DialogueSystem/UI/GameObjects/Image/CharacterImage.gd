extends FaceImage

class_name CharacterImage

signal LocationExit(character_image)
func location_exit()->void:
	self.emit_signal("LocationExit",self);

func is_interaction_set(value:bool)->void:
	.is_interaction_set(value);
	$Image.is_interaction_set(_is_interaction);

var _character:CharacterData setget _character_set, character_get;
func _character_set(value:CharacterData)->void:
	if _character:
		_character.disconnect("LocationExit",self,"location_exit");
	_character = value;
	if _character.connect("LocationExit",self,"location_exit") != OK:
		printerr('Character not connected signal to CharacterImage');
func character_get()->CharacterData:
	return _character;

func position_set(mode_flag:int)->void:
	$Image.position = $Image.texture.get_size()*_POSITIONS[mode_flag];

func init(character:CharacterData, 
		animation:String = '',
		mode_flag:int = FLOOR)->void:
	self._character_set(character);
	if animation:
		animation = animation+'.';
	var path:String = '%s%s%s'%[Config.CHARACTER_IMAGE_PATH,
								animation,
								character.image_get()];
	if Config.DEFAULT_DIRECTORY.file_exists(path):
		$Image.texture = load(path);
	else:
		$Image.texture = load('%s%s'%[Config.CHARACTER_IMAGE_PATH,
									character.image_get()]);
	self.position_set(mode_flag);

func _on_Image_mouse_click(_event:InputEvent)->void:
	if !self.character_get().messages_get().size():
		return;
	self.hide();
	self.emit_signal('object_select', self.character_get());

func is_outline_visible_set(value:bool)->void:
	$Image.is_show_outline_set(value);
