extends Node2D

class_name FaceImage

enum {
	FLOOR,
	CENTER,
	BASE
}

const _POSITIONS:Dictionary = {
	FLOOR:Vector2(-0.5,-1.0),
	CENTER:Vector2(-0.5,-0.5),
	BASE:Vector2(1.0,1.0),
}
# warning-ignore:unused_signal
signal object_select(object);

export var _is_interaction:bool = true setget is_interaction_set, is_interaction_get;
func is_interaction_set(value:bool)->void:
	_is_interaction = value;
func is_interaction_get()->bool:
	return _is_interaction;
