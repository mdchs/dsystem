extends Sprite

class_name SpriteOutline

signal mouse_click(event);

export var _is_interaction:bool = true setget is_interaction_set, is_interaction_get;
func is_interaction_set(value:bool)->void:
	_is_interaction = value;
	if _is_interaction == false:
		self.is_show_outline_set(false);
func is_interaction_get()->bool:
	return _is_interaction;

func _ready()->void:
	pass

func _unhandled_input(event)->void:
	if !self.is_interaction_get():
		return;
	if event is InputEventMouse and not event.is_echo():
		if self.is_pixel_has_uder_mouse():
			get_tree().set_input_as_handled();
			self.is_show_outline_set(true);
			self.mouse_click(event);
		else:
			self.is_show_outline_set(false);

func mouse_click(event:InputEventMouse)->void:
	if event is InputEventMouseButton and event.is_pressed():
			self.emit_signal("mouse_click",event);

func is_pixel_has_uder_mouse()->bool:
	var local:Vector2 = self.get_local_mouse_position();
	if self.get_rect().has_point(local):
		return true;
	return false;

var _is_show_outline:bool = false setget is_show_outline_set, is_show_outline_get;
func is_show_outline_set(value:bool)->void:
	if self.is_show_outline_get() == value:
		return;
	_is_show_outline = value;
	(self.material as ShaderMaterial).set_shader_param('is_show',_is_show_outline);
func is_show_outline_get()->bool:
	return _is_show_outline;
