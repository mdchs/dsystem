extends Control

signal transit_selected(location);
signal transitions_end;

const TRANSIT_BUTTON_PCK = preload("res://UI/Transits/TransitButton.tscn");
const TRANSIT_BUTTON_SIZE:int = 50;

func _ready():
	pass

func transit_load(location:LocationData)->void:
	self._clear();
	self._transit_load(location);
	self.show();
func transit_hide()->void:
	self.hide();

func _transit_load(location:LocationData)->void:
	for to_location in location.transits_get():
		var transit_button:TransitButton = TRANSIT_BUTTON_PCK.instance();
		$Panel/Scroll/Item.add_child(transit_button);
		transit_button.init(to_location, 
					location.transit_time_get(to_location));
		if transit_button.connect("transit_selected",self,"_transit_selected") != OK:
			print('not connect transit to location');
	$Panel.rect_size.y = $Panel.rect_min_size.y + location.transits_get().size()*TRANSIT_BUTTON_SIZE-5;

func _clear()->void:
	var list:GridContainer = $Panel/Scroll/Item;
	for child in list.get_children():
		child.queue_free();

func _on_ExitButton_pressed():
	self.emit_signal("transitions_end");

func _transit_selected(location:LocationData)->void:
	self.emit_signal('transit_selected',location);
	self._on_ExitButton_pressed();
