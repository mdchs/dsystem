extends Button

class_name TransitButton

signal transit_selected(location);

var _location:LocationData setget _location_set, location_get;
func _location_set(value:LocationData)->void:
	_location = value;
	
func location_get()->LocationData:
	return _location;

var _time:int setget _time_set, time_get;
func _time_set(value:int)->void:
	_time = value;
func time_get()->int:
	return _time;

func init(location:LocationData, time:int)->void:
	self._location_set(location);
	self._time_set(time);
	self.text = '%s: %s мин'%[self.location_get().name_get(),
						self.time_get()];

func _on_TransitButton_pressed()->void:
	CharacterStatic.player_get().location_move(
								self.location_get(),
								self.time_get());
	self.emit_signal("transit_selected",self.location_get());
