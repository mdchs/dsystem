extends Node2D

const _LOCATION_IMAGE_PKG:PackedScene = preload("res://UI/GameObjects/Image/LocationImage.tscn");

func _ready():
	if !LexerStatic.load_config():
		self.end_game();
		return;
	if !MainConfigParser.load_config():
		self.end_game();
		return;
	if !ItemConfigParser.load_config():
		self.end_game();
		return;
	if !ReceiptConfigParser.load_config():
		self.end_game();
		return;
	if !StuffConfigParser.load_config():
		self.end_game();
		return;
	if !LocationConfigParser.load_config():
		self.end_game();
		return;
	if !ProfessionConfigParser.load_config():
		self.end_game();
		return;
	if !MessageConfigParser.load_config():
		self.end_game();
		return;
	if !CharacterConfigParser.load_config():
		self.end_game();
		return;
	if !GOAPConfigLoader.load_config():
		self.end_game();
		return;
	StockExchangeStatic.bid_pool_load(300);
	GOAPStatic.gpath_pool_load(20);
	GOAPStatic.gagent_pool_load(300);
	TaskTrackerStatic.task_pool_load(500);
	FunctionPool.function_pool_load(1000);
	NeedsPool.needs_pool_load(200);
	self.location_load(CharacterStatic.player_get().location_get());
	for action in Config.START_ACTION_GET():
		action.exec();
	print('======');
	$Time.time_init();

func location_load(location:LocationData)->void:
	$Location.init(location);

func end_game()->void:
	pass;

func _location_path_get(location_name:String)->String:
	return 'res://UI/GameObjects/Locations/%s.tscn'%[location_name];
