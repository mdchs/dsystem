extends Timer

func _ready()->void:
	pass

func time_init()->void:
	self._on_Time_timeout();

func _on_Time_timeout()->void:
	SoulsContoller.autojob();
	TaskTrackerController.autojob();
	StockExchangeStatic._bids_sort();
	GlobalObject.time_inc();
	self.start();
