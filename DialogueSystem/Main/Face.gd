extends CanvasLayer

signal dialogue_end;
signal readboard_end;
signal transit_selected(location);

func _ready():
	pass

func object_select(object:GameObject)->void:
	if object is StuffData:
		return;
	$Dialogue.dialogue_show(object);
	$Dashboard.dashboard_hide();
func dialogue_hide()->void:
	self.emit_signal('dialogue_end');
	$Dashboard.dashboard_show();
	$Dialogue.dialogue_hide();

func transitions_show(location:LocationData)->void:
	$Transits.transit_load(location);
	$Dashboard.dashboard_hide();
func transit_selected(location:LocationData)->void:
	self.emit_signal("transit_selected", location);
	self.transitions_end();
func transitions_end()->void:
	$Dashboard.dashboard_show();
	$Transits.transit_hide();

func inventory_show(inventory:Inventory)->void:
	$Inventory.inventory_show(inventory);
	$Dashboard.dashboard_hide();
func inventory_hide()->void:
	$Inventory.inventory_hide();
	$Dashboard.dashboard_show();

func readboard_show(nme:String, text:String)->void:
	$ReadBoard.readboard_show(nme, text);
	$Dashboard.dashboard_hide();

func readboard_hide()->void:
	self.emit_signal('readboard_end');
	$ReadBoard.readboard_hide();
	$Dashboard.dashboard_show();
